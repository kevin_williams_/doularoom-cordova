Project
=====================


### Install Project Dependencies

Install Git - http://git-scm.com/downloads
Install Nodejs - http://nodejs.org/download/

Install Ionic

Ionic comes with a convenient command line utility to start, build, and package Ionic apps. To install it, simply run:

```
$ sudo npm install -g ionic
```


### Testing the hybrid application

Use Bower to pull the latest levels of the Mobile Cloud SDKs. From the sample app directory, run:

```
bower install
```

To check the development runtime, make sure all running Google Chrome processes are shut down. Then load Google Chrome with web security turned off, which enables the mobile testing to avoid any cross-origin resource sharing.

From a command line on Macintosh:

```
open -a Google\ Chrome --args --disable-web-security
```

From a command line on Windows:

```
start chrome.exe --disable-web-security
```

Run the application. From the app directory, run:

```
ionic serve
```

View the hybrid component of the app. The Chrome browser live reloads the hybrid component automatically. You can also go to the following URL:

```
http://localhost:8100/
```

Run the application on a mobile device or emulator. If you want to test with iOS, add the iOS platform to the project:

```
ionic platform add ios
```

Build it uisng the following command:

```
ionic build ios
```

Test the application. You can emulate an iOS environment with the following command:

```
ionic emulate ios
```
