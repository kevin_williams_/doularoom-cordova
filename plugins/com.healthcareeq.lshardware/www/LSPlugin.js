var pluginName = "LSPlugin";

var LSPlugin = {

  startScan: function(successCallback) {
    cordova.exec(successCallback, function(err){}, pluginName, "startScan", []);
  },
  stopScan: function(successCallback) {
    cordova.exec(successCallback, function(err){}, pluginName, "stopScan", []);
  },
  retrievePairedDevices: function(successCallback) {
    cordova.exec(successCallback, function(err){}, pluginName, "retrievePairedDevices", []);
  },
  forgetPairedDevices: function(successCallback) {
    cordova.exec(successCallback, function(err){}, pluginName, "forgetPairedDevices", []);
  },
  test: function() {
    return "Success!";
  }
  
};

module.exports = LSPlugin;
