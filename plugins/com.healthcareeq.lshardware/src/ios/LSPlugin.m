#import "LSPlugin.h"


@implementation LSPlugin


- (PedometerUserDownLoadInfo *)hardwareConnectorGetPedometerDownloadInfoForSensor:(LSHardwareSensor *)sensor {
    PedometerUserDownLoadInfo *info = [PedometerUserDownLoadInfo new];
    info.height = 1.70;
    info.stride = 0.78;
    info.weekStart = 1;
    info.weekTargetCalories = 0;
    info.weekTargetDistance = 0;
    info.weekTargetExerciseAmount = 2;
    info.weekTargetSteps = 14000;
    info.weight = 65.5;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Configure pedometer download information");
    });

    return info;
}

// Start scanning for devices
- (void)startScan:(CDVInvokedUrlCommand*)command {
    if (scanCallback != nil)
    {
        return;
    }

    scanCallback = command.callbackId;
    NSLog(@"Scanning Started");
    NSString* status = @"Start Scanning";
    NSString* action = @"scanning";

    NSMutableDictionary* returnObject = [NSMutableDictionary dictionaryWithObjectsAndKeys: status, @"status", action, @"action", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:true];

    [[LSHardwareConnector shareConnector] startScanningWithDelegate:self withEnalbeSensorTypes:@[
        [NSNumber numberWithInt:LS_SENSOR_TYPE_PEDOMETER],
        [NSNumber numberWithInt:LS_SENSOR_TYPE_WEIGHT_SCALE],
        [NSNumber numberWithInt:LS_SENSOR_TYPE_GENERAL_WEIGHT_SCALE],
        [NSNumber numberWithInt:LS_SENSOR_TYPE_BLOODPRESSURE]
    ]];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];
}

// Stop scanning for devices
- (void)stopScan:(CDVInvokedUrlCommand*)command {
    NSString *action = @"scanning-stopped";
    NSString *status = @"Scanning Stopped";
    NSLog(@"Scanning Stopped");

    if (scanCallback == nil)
    {
        return;
    }

    scanCallback = nil;

    NSMutableDictionary* returnObject = [NSMutableDictionary dictionaryWithObjectsAndKeys: status, @"status", action, @"action", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:false];
    [[LSHardwareConnector shareConnector] cancelScanning];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

// Invoked if a device was found
-(void)pairConnectorDiscoveredPairingSensor:(LSHardwareSensor*)sensor {
    NSLog(@"hardwareConnector Discovered Pairing Sensor");

    NSString *action = @"device-found";
    NSString *status = @"Device Found";

    /*NSLog(@"sensor: %@", sensor);
    NSLog(@"deviceId: %@", sensor.deviceId);
    NSLog(@"manufactureName: %@", sensor.manufactureName);
    NSLog(@"sensorName: %@", sensor.sensorName);
    NSLog(@"modelNumber: %@", sensor.modelNumber);*/

    NSMutableDictionary *device = [NSMutableDictionary
    dictionaryWithDictionary:@{
      @"sensorName" : sensor.sensorName,
      @"modelNumber" : sensor.modelNumber
    }];

    /*NSMutableDictionary* returnObject = [NSMutableDictionary
      dictionaryWithObjectsAndKeys: status, @"status", action, @"action", nil];*/
    NSMutableDictionary* returnObject = [NSMutableDictionary
                                         dictionaryWithObjectsAndKeys: status, @"status", action, @"action", device, @"device", nil];
    NSLog(@"device: %@", device);
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:true];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];

    [[LSHardwareConnector shareConnector] pairWithHardwareSensor:sensor];

}

// Invoked when pairing device
-(void)pairConnectorPairedSensor:(LSHardwareSensor*)sensor withState:(BOOL)state {

    NSLog(@"hardwareConnector Paired Sensor");
    NSString *action = @"device-paired";
    NSString *status = @"Device Paired Successfully";

    NSMutableDictionary* returnObject = [NSMutableDictionary
      dictionaryWithObjectsAndKeys: status, @"status", action, @"action", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:true];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];
}

// Invoked when weight data is available
-(void)hardwareConnectorReceiveWeightMeasurementData:(WeightData*)data {

  NSLog(@"Weight scale data received");
  NSString *action = @"weight-scale-data";
  NSString *status = @"Weight Scale Data";
  NSMutableDictionary *deviceData = [NSMutableDictionary
  dictionaryWithDictionary:@{
    @"date" : data.date,
    @"weight" : [NSNumber numberWithDouble:data.weight],
    @"pbf" : [NSNumber numberWithDouble:data.pbf]
  }];

  NSMutableDictionary* returnObject = [NSMutableDictionary
    dictionaryWithObjectsAndKeys: status, @"status", action, @"action", deviceData, @"deviceData", nil];
  CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
  [pluginResult setKeepCallbackAsBool:true];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];

}

// Invoked when pedometer data is available
-(void)hardwareConnectorReceivePedometerMeasurementData:(PedometerData*)data {

    NSLog(@"Pedometer data received");
    NSString *action = @"pedometer-data";
    NSString *status = @"Pedometer Data";
    NSMutableDictionary *deviceData = [NSMutableDictionary
    dictionaryWithDictionary:@{
      @"date" : data.date,
      @"walked" : [NSNumber numberWithLong:data.walkSteps],
      @"runned" : [NSNumber numberWithLong:data.runSteps],
      @"examount" : [NSNumber numberWithDouble:data.examount],
      @"calories" : [NSString stringWithFormat:@"%ld kcal",(long)data.calories], /*the unit is kilogram calories*/
      @"exerciseTime" : [NSString stringWithFormat:@"%ld min",(long)data.exerciseTime],   /*the unit is minute */
      @"distance" : [NSString stringWithFormat:@"%ld m",(long)data.distance],  /*the unit is meter*/
      @"battery" : [NSNumber numberWithLong:data.battery],
      @"sleepStatus" : [NSNumber numberWithLong:data.sleepStatus],
      @"intensityLevel" : [NSNumber numberWithLong:data.intensityLevel]
    }];

    NSLog(@"data: %@", deviceData);

    NSMutableDictionary* returnObject = [NSMutableDictionary
      dictionaryWithObjectsAndKeys: status, @"status", action, @"action", deviceData, @"deviceData", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:true];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];
}

// Invoked when blood pressure data is available
-(void)hardwareConnectorReceiveBloodPressureMeasurementData:(BloodPressureData *)data {

    NSLog(@"Blood pressure data received");
    NSString *action = @"blood-pressure-data";
    NSString *status = @"Blood Pressure Data";
    NSMutableDictionary *deviceData = [NSMutableDictionary
    dictionaryWithDictionary:@{
      @"date" : data.date,
      @"isIrregularPulse" : [NSNumber numberWithBool:data.isIrregularPulse],
      @"battery" : [NSNumber numberWithLong:data.battery],
      @"bloodPressure" : [NSString stringWithFormat:@"%ld/%ld mmHg",(long)data.systolic, (long)data.diastolic],
      @"heartRate" : [NSString stringWithFormat:@"%ld bpm",(long)data.pluseRate]
    }];

    NSMutableDictionary* returnObject = [NSMutableDictionary
      dictionaryWithObjectsAndKeys: status, @"status", action, @"action", deviceData, @"deviceData", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [pluginResult setKeepCallbackAsBool:true];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:scanCallback];

}

// Invoked when kitchen scale data is available
-(void)hardwareConnectorReceiveKitchenScaleMeasurementData:(KitchenScaleData*)data{
    NSLog(@"Kitchen scale data received");
}

// Invoked when height data is available
-(void)hardwareConnectorReceiveHeightMeasurementData:(HeightData*)data {
    NSLog(@"Height data received");
}

// Invoked when general weight data is available
-(void)hardwareConnectorReceiveGeneralWeightMeasurementData:(WeightData*)data {
    NSLog(@"Weight scale data received");
}

// Invoked to retrieve all paired devices
- (void) retrievePairedDevices:(CDVInvokedUrlCommand*)command {

  NSLog(@"Paired Devices List");
  NSString *action = @"retrieve-paired-devices";
  NSString *status = @"Retrieve Paired Devices";
  NSMutableArray *devices = [NSMutableArray array];
  NSArray *pairedDevices = [[LSHardwareConnector shareConnector] pairedSensors];
  NSNumber *numOfDevices = [NSNumber numberWithInt:(unsigned long)pairedDevices.count];

  for (LSHardwareSensor *sensor in pairedDevices) {
      [devices addObject:sensor.sensorName];
  }

  NSMutableDictionary* returnObject = [NSMutableDictionary
    dictionaryWithObjectsAndKeys: status, @"status", action, @"action", devices, @"devices", numOfDevices, @"numOfDevices", nil];
  CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

// Invoked to forget all paired devices
- (void) forgetPairedDevices:(CDVInvokedUrlCommand*)command {

    NSLog(@"Forget Paired Devices List");
    NSString *action = @"forget-paired-devices";
    NSString *status = @"Forget Paired Devices";
    NSArray *pairedDevices = [[LSHardwareConnector shareConnector] pairedSensors];

    for (LSHardwareSensor *sensor in pairedDevices) {
        [[LSHardwareConnector shareConnector] forgetPairedSensorWithSensor:sensor];
    }

    NSMutableDictionary* returnObject = [NSMutableDictionary
      dictionaryWithObjectsAndKeys: status, @"status", action, @"action", nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:returnObject];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

@end
