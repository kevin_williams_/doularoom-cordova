#import <Cordova/CDV.h>
#import <LSBLE__A2/LSBLE_A2.h>


@interface LSPlugin : CDVPlugin <LSHardwareConnectorDelegate>
{
    NSString* scanCallback;
}


// Cordova plugin interface
- (void) startScan:(CDVInvokedUrlCommand*)command;
- (void) stopScan:(CDVInvokedUrlCommand*)command;
- (void) retrievePairedDevices:(CDVInvokedUrlCommand*)command;
- (void) forgetPairedDevices:(CDVInvokedUrlCommand*)command;


@end
