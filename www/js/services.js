/*
 * Copyright 2014 IBM Corp. All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

angular.module('starter.services', [])

/**
 * A Service that intialises MBaaS
 */
//New bm_utils
    .factory('bm_utils',
	    function () {

	        return {
	            uniqueItems: function (_array, _meta) {

	                // Copy the Attributes out for easy consumption
	                var we = [];
	                _array.forEach(function (w) {
	                    we.push(w.attributes);
	                    if (_meta) {
	                        we.push(w._meta);
	                    }
	                });

	                // resolve down to unique items
	                var uniqueList = _.uniq(we, function (item, key, a) {
	                    return item.a;
	                });

	                return we;

	            }
	        }

	    })
    //End New bm_utils
  /**
   * Patient-Roster Data.
   */
  //--
.factory('LoginService', function ($rootScope, $q, $location, $cacheFactory, bm_utils) {

    // Use an internal Cache for storing the List and map the operations to manage that from
    // Mobile Cloud SDK Calls
    var cache = $cacheFactory('');

    // Static Constants look to move into Angular Constants
    var JSONClassName_USERS = 'USERS';
    return {

        getCredentials: function (username, password) {
            return this.getData(username, password, false);
        },

        // Return all the Objects for a Given Class
        getData: function (username, password, meta) {

            // Create a Defer as this is an async operation
            var defer = $q.defer();
            //console.log(password);
            var ibmdata = IBMData.getService();
            var encryptedPassword = md5(password);
            //alert(encryptedPassword);
            // Query for the Data 
            ibmdata.Query.ofType(JSONClassName_USERS)
            .find({
                USERNAME: username,
                PASSWORD: encryptedPassword
            }).then(function (_data) {
                // Data
                console.log(JSON.stringify(_data));
                var testData = JSON.parse(JSON.stringify(_data));
                //alert(testData[0].attributes.ROLE);
                var data = bm_utils.uniqueItems(_data, meta);
                //alert(_data.ROLE);
                if (_data.length == 1) {
                    console.log("My Login Successful");
                    //defer.resolve(cache.get(data));

                    defer.resolve(testData);
                    //return data;
                }
                else {
                    //alert("Password: " + username);
                    $("#passwordError").html("The Password/Username Combination you used is incorrect...");
                }

            }, function (err) {
                console.log(err);
                defer.reject(err);
            });
            return defer.promise;
        }
    }
})

  .factory('patient_roster_Service', function ($rootScope, $q, $cacheFactory, bm_utils) {

      // Use an internal Cache for storing the List and map the operations to manage that from
      // Mobile Cloud SDK Calls
      var cache = $cacheFactory('chain');

      // Static Constants look to move into Angular Constants
      var JSONClassName_Patients = 'PATIENTS';
      //var JSONClassName_Symptoms = 'SYMPTOMS';
      //var JSONClassName_Nutrition = 'NUTRITION_INTAKE';
      //var JSONClassName_NutritionDaily = 'NUTRITION_INTAKE_DAILY';
      var JSONClassName_TestResults = 'TEST_RESULTS';
      var JSONClassName_PatientRoster = 'PATIENT_ROSTER';
      //ar JSONClassName_PatSym = 'PAT_SYM';

      return {



          getPhysiciansPatients: function (pid, physician_id) {
              return this.getData(pid, physician_id, false);
          },

          //For testing
          getRoster: function (physician_id) {
              return this.getPatientRoster2(physician_id);
          },

          //Original
          getRoster2: function (physician_id) {
              return this.getPatientRoster(physician_id, false);
          },

          getPatientRoster2: function (patientID) {
              var defer = $q.defer();
              var ibmData = IBMData.getService();

              ibmData.Query.ofType(JSONClassName_Patients)
              .find(/*{ PATIENTID: patientID} */)
              .then(function (_result) {

                  var returnedObj = {};
                  returnedObj.patients = [];
                  returnedObj.isHighRisk = "";
                  _result.forEach(function (patient) {

                      if (patient.RF_HYPER == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_PREV_UTER_SURG == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_DRUG_USE == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_STD_STI == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_OVR_35_YOA == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_DIABETES == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_PFMG == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_PFD == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_HIV == 0) {
                          returnedObj.isHighRisk = "N";
                      } else if (patient.RF_SICKLE_CELL == 0) {
                          returnedObj.isHighRisk = "N";
                      } else {
                          returnedObj.isHighRisk = "Y";
                      }




                      returnedObj.patients.push(patient.attributes);
                  });
                  return returnedObj;

                  //End of getting patients data.

              }).then(function (_result) { //Beginning of getting Blood Pressure data

                  var returnedObj = _result;

                  return ibmData.Query.ofType(JSONClassName_TestResults)
                  .find({ PATIENTID: patientID, TYPE_OF_TEST: "blood_pressure" })
                  .then(function (_result) {

                      _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
                      returnedObj.blood_pressure_result = [];

                      returnedObj.blood_pressure_result.push(_result[_result.length - 1].attributes);
                      console.log("bp", returnedObj.blood_pressure_result);

                      return returnedObj;

                  });  //End of getting Blood Pressure data

              }).then(function (_result) { //Beginning of getting water-intake data

                  var returnedObj = _result;

                  return ibmData.Query.ofType(JSONClassName_TestResults)
                  .find({ PATIENTID: patientID, TYPE_OF_TEST: "water_intake" })
                  .then(function (_result) {

                      _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
                      returnedObj.water_intake_result = [];

                      returnedObj.water_intake_result.push(_result[_result.length - 1].attributes);
                      console.log("wi", returnedObj.water_intake_result);

                      return returnedObj;

                  });  //End of getting water-intake data

              }).then(function (_result) { //Beginning of getting heart-rate data

                  var returnedObj = _result;

                  return ibmData.Query.ofType(JSONClassName_TestResults)
                  .find({ PATIENTID: patientID, TYPE_OF_TEST: "heart_rate" })
                  .then(function (_result) {

                      _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
                      returnedObj.heart_rate_result = [];

                      returnedObj.heart_rate_result.push(_result[_result.length - 1].attributes);
                      console.log("hr", returnedObj.heart_rate_result);

                      return returnedObj;

                  });  //End of getting heart-rate data

              }).then(function (_result) { //Beginning of getting weight data

                  var returnedObj = _result;

                  return ibmData.Query.ofType(JSONClassName_TestResults)
                  .find({ PATIENTID: patientID, TYPE_OF_TEST: "weight" })
                  .then(function (_result) {

                      _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
                      returnedObj.weight_result = [];

                      returnedObj.weight_result.push(_result[_result.length - 1].attributes);
                      console.log("weight", returnedObj.weight_result);

                      return returnedObj;

                  });  //End of getting weight data

              }).then(function (_result) { //Beginning of getting steps data

                  var returnedObj = _result;

                  return ibmData.Query.ofType(JSONClassName_TestResults)
                  .find({ PATIENTID: patientID, TYPE_OF_TEST: "steps" })
                  .then(function (_result) {

                      _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
                      returnedObj.steps_result = [];

                      returnedObj.steps_result.push(_result[_result.length - 1].attributes);
                      console.log("weight", returnedObj.steps_result);

                      return returnedObj;

                  });  //End of getting steps data

              }).then(function (_result) {

                  var returnedObj1 = [];
                  _result.patients.forEach(function (patient) {
                      //Blood-Pressure Data
                      var test_results = _.filter(_result.blood_pressure_result, function (test_result) {
                          return patient.PATIENTID === test_result.PATIENTID;
                      });
                      patient.test_results = test_results;

                      //Water-Intake Data
                      var water_intake_results = _.filter(_result.water_intake_result, function (test_result) {
                          return patient.PATIENTID === test_result.PATIENTID;
                      });
                      patient.water_intake_results = water_intake_results;

                      //Patient Heart-Rate Data
                      var heart_rate_results = _.filter(_result.heart_rate_result, function (test_result) {
                          return patient.PATIENTID === test_result.PATIENTID;
                      });
                      patient.heart_rate_results = heart_rate_results;

                      //Weight Data
                      var weight_results = _.filter(_result.weight_result, function (test_result) {
                          return patient.PATIENTID === test_result.PATIENTID;
                      });
                      patient.weight_results = weight_results;

                      //Steps Data
                      var steps_results = _.filter(_result.steps_result, function (test_result) {
                          return patient.PATIENTID === test_result.PATIENTID;
                      });
                      patient.steps_results = steps_results;

                      returnedObj1.push(patient);
                  });
                  console.log("returnedObj: " + JSON.stringify(returnedObj1));
                  return defer.resolve(returnedObj1);

              }, function (err) {
                  console.log(err);
                  defer.reject(err);
              });

              return defer.promise;
          },


          // Return all the Objects for a Given Class
          getData: function (pid, physicianid, meta) {

              // Create a Defer as this is an async operation
              var defer = $q.defer();

              // Check if we have any and then pull it out of the Cache if we do
              if (_.isUndefined(cache.get(physicianid))) {

                  // get the Data Service
                  var ibmdata = IBMData.getService();

                  // Query for the Data
                  ibmdata.Query.ofType(JSONClassName_Patients)
                  .find({
                      PATIENTID: pid,
                      PHYSICIANID: physicianid
                  }).then(function (_data) {

                      // Data
                      var data = bm_utils.uniqueItems(_data, meta);
                      cache.put(physicianid, data);
                      defer.resolve(cache.get(physicianid));

                  }, function (err) {
                      console.log(err);
                      defer.reject(err);
                  });

              } else {
                  defer.resolve(cache.get(physicianid));
              }

              // Get the Objects for a particular Type
              return defer.promise;

          },

          // Return all the Objects for a Given Class
          getData2: function () {

              // Create a Defer as this is an async operation
              var defer = $q.defer();

              // Check if we have any and then pull it out of the Cache if we do
              //if(_.isUndefined(cache.get(physicianid))) {

              // get the Data Service
              var ibmdata = IBMData.getService();

              // Query for the Data
              ibmdata.Query.ofType(JSONClassName_PatientRoster)
              .find().then(function (_data) {

                  _data = _.sortBy(_data, function (obj) { return obj.attributes.PATIENTID; });
                  //console.log("_data", _data);
                  // Data
                  var data = bm_utils.uniqueItems(_data, false);
                  cache.put(data);
                  defer.resolve(data);

              }, function (err) {
                  console.log(err);
                  defer.reject(err);
              });

              /*} else {
                  defer.resolve(cache.get(physicianid));
              }*/

              // Get the Objects for a particular Type
              return defer.promise;

          },

          // Return all patients for the logged in physician.
          getPatientRoster: function (physicianid, meta) {

              // Create a Defer as this is an async operation
              var defer = $q.defer();

              // Check if we have any and then pull it out of the Cache if we do
              if (_.isUndefined(cache.get(physicianid))) {

                  // get the Data Service
                  var ibmdata = IBMData.getService();

                  // Query for the Data
                  ibmdata.Query.ofType(JSONClassName_Patients)
                  .find({
                      PHYSICIANID: physicianid
                  }).then(function (_data) {

                      //---------------------------------------------------------------
                      /*var _groupedPatientData = [];
                      _data.forEach(function(patientData){

                         _groupedPatientData.push(patientData.attributes); 

                       });

                      //var _uniquePatientData =   bm_utils.uniqueItems(_groupedPatientData,meta);
                      //cache.put(physicianid, _uniquePatientData);
                      defer.resolve(_groupedPatientData);
                      //console.log(_groupedPatientData);*/
                      //---------------------------------------------------------------

                      // Data
                      var data = bm_utils.uniqueItems(_data, meta);
                      cache.put(physicianid, data);
                      defer.resolve(cache.get(physicianid));
                      console.log(_data);

                  }, function (err) {
                      console.log(err);
                      defer.reject(err);
                  });

              } else {
                  defer.resolve(cache.get(physicianid));
              }

              // Get the Objects for a particular Type
              return defer.promise;

          }

      } //End return
  })

//--

.factory('patient_vitamins_Service', function ($rootScope, $q, $cacheFactory, bm_utils) {

    // Use an internal Cache for storing the List and map the operations to manage that from
    // Mobile Cloud SDK Calls
    var cache = $cacheFactory('vitamins');

    // Static Constants look to move into Angular Constants
    //var JSONClassName_Patients = 'PATIENTS';
    //var JSONClassName_Symptoms = 'SYMPTOMS';
    //var JSONClassName_Nutrition = 'NUTRITION_INTAKE';
    var JSONClassName_NutritionDaily = 'NUTRITION_INTAKE_DAILY';
    //var JSONClassName_TestResults = 'TEST_RESULTS';
    //ar JSONClassName_PatSym = 'PAT_SYM';

    return {

        getPatientVitamins: function (pid) {
            //return this.getData(pid, physicianid, false);
            //return this.getPatientVitaminInfo(pid, false);
            //return this.getVitaminsInfo(pid);
            console.log("Patient-ID called: " + pid);
            return this.getVitaminsInformation(pid);
        },

        getVitaminsInfo: function (patientID) {
            var defer = $q.defer();
            var ibmData = IBMData.getService();

            ibmData.Query.ofType(JSONClassName_NutritionDaily)
            .find({ PATIENTID: patientID })
            .then(function (_result) {

                var returnedObj = {};
                returnedObj.patientsVitamins = [];

                _result.forEach(function (vitamins) {

                    returnedObj.patientsVitamins.push(vitamins.attributes);
                    console.log("patientsVitamins", returnedObj.patientsVitamins);

                })
                console.log("returnedObj: " + JSON.stringify(returnedObj));
                defer.resolve(returnedObj);


            }, function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        },
        // Return all the Objects for a Given Class
        getVitaminsInformation: function (patientID) {

            // Create a Defer as this is an async operation
            var defer = $q.defer();

            // Check if we have any and then pull it out of the Cache if we do
            //if(_.isUndefined(cache.get(physicianid))) {

            // get the Data Service
            var ibmdata = IBMData.getService();
            console.log("Patient-ID clicked: " + patientID);

            // Query for the Data
            ibmdata.Query.ofType(JSONClassName_NutritionDaily)
            //.find({"PATIENTID":"PAT100"}).then(function(_data) {                   
              .find({ "PATIENTID": patientID }).then(function (_data) {

                  _data = _.sortBy(_data, function (obj) { return obj.attributes.TIME_RECORDED; });
                                   
                  // Data
                  var data = bm_utils.uniqueItems(_data, false);
                  cache.put(data);
                  defer.resolve(data);

              }, function (err) {
                  console.log(err);
                  defer.reject(err);
              });

            /*} else {
                defer.resolve(cache.get(physicianid));
            }*/

            // Get the Objects for a particular Type
            return defer.promise;

        },

        // Return all patients for the logged in physician.
        getPatientVitaminInfo: function (patientID, meta) {

            // Create a Defer as this is an async operation
            var defer = $q.defer();

            // Check if we have any and then pull it out of the Cache if we do
            if (_.isUndefined(cache.get(patientID))) {

                // get the Data Service
                var ibmdata = IBMData.getService();

                // Query for the Data
                ibmdata.Query.ofType(JSONClassName_NutritionDaily)
                .find({
                    PATIENTID: patientID
                }).then(function (_data) {

                    //---------------------------------------------------------------
                    /*var _groupedPatientData = [];
                    _data.forEach(function(patientData){

                       _groupedPatientData.push(patientData.attributes); 

                     });

                    //var _uniquePatientData =   bm_utils.uniqueItems(_groupedPatientData,meta);
                    //cache.put(patientID, _uniquePatientData);**
                    defer.resolve(_groupedPatientData);
                    //console.log(_groupedPatientData);*/
                    //---------------------------------------------------------------

                    // Data
                    var data = bm_utils.uniqueItems(_data, meta);
                    cache.put(patientID, data);
                    defer.resolve(cache.get(patientID));
                    console.log(_data);

                }, function (err) {
                    console.log(err);
                    defer.reject(err);
                });

            } else {
                defer.resolve(cache.get(patientID));
            }

            // Get the Objects for a particular Type
            return defer.promise;

        },

        // Return all the Objects for a Given Class
        getData: function (pid, physicianid, meta) {

            // Create a Defer as this is an async operation
            var defer = $q.defer();

            // Check if we have any and then pull it out of the Cache if we do
            if (_.isUndefined(cache.get(physicianid))) {

                // get the Data Service
                var ibmdata = IBMData.getService();

                // Query for the Data
                ibmdata.Query.ofType(JSONClassName_NutritionDaily)
                .find({
                    PATIENTID: pid,
                    PHYSICIANID: physicianid
                }).then(function (_data) {

                    // Data
                    var data = bm_utils.uniqueItems(_data, meta);
                    cache.put(physicianid, data);
                    defer.resolve(cache.get(physicianid));

                }, function (err) {
                    console.log(err);
                    defer.reject(err);
                });

            } else {
                defer.resolve(cache.get(physicianid));
            }

            // Get the Objects for a particular Type
            return defer.promise;

        }



    } //End return
})

//--


.factory('patient_weight_Service', function ($rootScope, $q, $cacheFactory, bm_utils) {

    // Use an internal Cache for storing the List and map the operations to manage that from
    // Mobile Cloud SDK Calls
    var cache = $cacheFactory('all_weight');

    // Static Constants look to move into Angular Constants
    //var JSONClassName_Patients = 'PATIENTS';
    //var JSONClassName_Symptoms = 'SYMPTOMS';
    //var JSONClassName_Nutrition = 'NUTRITION_INTAKE';
    //var JSONClassName_NutritionDaily = 'NUTRITION_INTAKE_DAILY';
    var JSONClassName_TestResults = 'TEST_RESULTS';
    //ar JSONClassName_PatSym = 'PAT_SYM';

    return {



        getPatientWeight: function (pid) {

            return this.getWeightInformation();
        },


        // Return all the Objects for a Given Class
        getWeightInformation: function () {

            // Create a Defer as this is an async operation
            var defer = $q.defer();

            // Check if we have any and then pull it out of the Cache if we do
            //if(_.isUndefined(cache.get(physicianid))) {

            // get the Data Service
            var ibmdata = IBMData.getService();

            // Query for the Data
            ibmdata.Query.ofType(JSONClassName_TestResults)
            .find({ "PATIENTID": "PAT100", "TYPE_OF_TEST": "weight" }).then(function (_data) {

                // Data
                var data = bm_utils.uniqueItems(_data, false);
                cache.put(data);
                defer.resolve(data);

            }, function (err) {
                console.log(err);
                defer.reject(err);
            });

            /*} else {
                defer.resolve(cache.get(physicianid));
            }*/

            // Get the Objects for a particular Type
            return defer.promise;

        }


    } //End return
})

//page_under_construction_Service
.factory('page_under_construction_Service', function ($rootScope, $q, $cacheFactory, bm_utils) {

    // Use an internal Cache for storing the List and map the operations to manage that from
    // Mobile Cloud SDK Calls
    var cache = $cacheFactory('all_weight');

        
})


/**
 * A Service that intialises MBaaS
 */
.factory('InitBluemix',
    function($rootScope, $http, $q) {

        function alertNotification (message) {

            alert(JSON.stringify(message));
            $rootScope.message = message;

        };

        function init() {

            // Create a defer
            var defer = $q.defer();

            // Lets load the Configuration from the bluelist.json file
            $http.get("./bluemix.json").success(function(config) {

                // Initialise the SDK
                IBMBluemix.initialize(config).done(function() {

                    // Let the user no they have logged in and can do some stuff if they require
                    console.log("Sucessful initialisation with Application : " + IBMBluemix.getConfig().getApplicationId());

                    // Initialize the Service
                    var data = IBMData.initializeService();

                    // Let the user no they have logged in and can do some stuff if they require
                    console.log("Sucessful initialisation Data Services " );

                    // Check we are in Cordova
                    if(window.cordova) {
                        IBMBluemix.hybrid.initialize(config). then(function() {
                            console.log("initialize Hybrid completed successfully");
                            return IBMPush.hybrid.initializeService();
                        }). then(function (push) {
                            console.log("Push Push Service initialized");
                            return push.registerDevice("CordovaDevice","User1", alertNotification);
                        });
                    }

                    // Return
                    defer.resolve();


                }, function(response) {
                    // Error
                    console.log("Error:", response);
                    defer.reject(response);
                });

                $rootScope.config = config;;
            });

            return defer.promise;

        };

        return {
            init: function() {
                return init();
            }
        }

    });
