//$(document).ready(function(){

  // date element
  $('#txtLastMenstrualCycle').datepicker({
    format: "mm-dd-yyyy",
    startView: 1,
    daysOfWeekDisabled: "3,4",
    autoclose: true,
    todayHighlight: true
  });

  $('#txtDateOfBirth').datepicker({
    format: "mm-dd-yyyy",
    startView: 1,
    daysOfWeekDisabled: "3,4",
    autoclose: true,
    todayHighlight: true
  });

  //Form Wizard Validations
  var $validator = $("#commentForm").validate({
   rules: {
     txtFirstName: {required: true, minlength: 3},
     txtLastName: {required: true, minlength: 3},
     txtDateOfBirth: {required: true, minlength: 3},
     txtOccupation: {required: true, minlength: 3},
     txtEmail: {required: true, minlength: 3},
     txtAddress: {required: true, minlength: 3},
     txtCity: {required: true, minlength: 3},
     txtState: {required: true, minlength: 2},
     txtCountry: {required: true, minlength: 3},
     txtPostalCode: {required: true, minlength: 3},
     txtTeleNo: {required: true, minlength: 3},
     txtHospitalName: {required: true, minlength: 3},
     txtHospitalAddress: {required: true, minlength: 3},
     txtHospitalCity: {required: true, minlength: 3},
     txtHospitalState: {required: true, minlength: 2},
     txtHospitalCountry: {required: true, minlength: 3},
     txtHospitalPostalCode: {required: true, minlength: 3},
     txtHospitalTeleNo: {required: true, minlength: 3}
   },
   errorPlacement: function(label, element) {
     $('<span class="arrow"></span>').insertBefore(element);
     $('<span class="error"></span>').insertAfter(element).append(label)
   }
  });

  // Bootstrap wizard
  $('#rootwizard').bootstrapWizard({
     'tabClass': 'form-wizard',
     'onNext': function(tab, navigation, index) {
        var $valid = $("#commentForm").valid();
        var $total = navigation.find('li').length;
        var $current = index + 1;

        if(!$valid) {
          $validator.focusInvalid();
          return false;
        }
        else {
          $('#rootwizard').find('.form-wizard').children('li').eq(index-1).addClass('complete');
          $('#rootwizard').find('.form-wizard').children('li').eq(index-1).find('.step').html('<i class="fa fa-check"></i>');
          // If it's the last tab then hide the last button and show the finish instead
      		if($current === $total) {
      			$('#rootwizard').find('.wizard .next').hide();
      			$('#rootwizard').find('.wizard .finish').show();
      			$('#rootwizard').find('.wizard .finish').removeClass('disabled');
      		} else {
      			$('#rootwizard').find('.wizard .next').show();
      			$('#rootwizard').find('.wizard .finish').hide();
      		}
        }
      },
      'onPrevious': function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        if ($current < $total) {
          $('#rootwizard').find('.wizard .next').show();
          $('#rootwizard').find('.wizard .finish').hide();
        }

      }
  });


//});
