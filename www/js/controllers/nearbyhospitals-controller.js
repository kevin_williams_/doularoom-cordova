angular.module('doularoom.controllers.nearbyhospitals', [])

.controller('NearbyHospitalsCtrl', function ($scope) {

    var map,
      infowindow,
      circle,
      rad = 2000;

    function initialize() {
        var mapOptions = { zoom: 13 };
        map = new google.maps.Map(document.getElementById('google_map'), mapOptions);
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(pos);
            var request = {
                location: pos,
                radius: rad,
                types: ['hospital']
            };
            var placeOptions = {
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#FF0000',
                fillOpacity: 0,
                map: map,
                center: pos,
                radius: rad
            };
            circle = new google.maps.Circle(placeOptions);

            infowindow = new google.maps.InfoWindow();
            var service = new google.maps.places.PlacesService(map);
            service.nearbySearch(request, callback);
        });
    }

    function callback(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
        }
    }

    function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }

    $scope.$on('$viewContentLoaded', function () {
        initialize();
    });

});
