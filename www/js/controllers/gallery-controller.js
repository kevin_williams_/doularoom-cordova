angular.module('doularoom.controllers.gallery', [])

.controller('GalleryCtrl', function($rootScope, $scope, $location, $ionicModal, InitBluemix, PATIENT, GalleryService) {


  $scope.gallery = null;
  $scope.newGallery = {};

  $ionicModal.fromTemplateUrl('templates/modals/edit-gallery.html', {
      scope: $scope
  }).then(function(modal) {
      $scope.modal = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modals/add-gallery.html', {
      scope: $scope
  }).then(function(modal) {
      $scope.addModal = modal;
  });

  $scope.editGallery = function(gallery) {
      $scope.modal.show();
      $scope.gallery = gallery;
  };

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.loadGallery = function() {

      GalleryService.getGalleries(PATIENT.PATIENTID)
      .then(function(galleriesData) {
        $scope.galleriesData = galleriesData;
        //console.log("galleriesData: " + JSON.stringify(galleriesData));
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });
  };

  $scope.updateGallery = function() {

      GalleryService.updateGallery($scope.gallery)
      .then(function(gallery) {
        //console.log("gallery: " + JSON.stringify(gallery));
        $scope.modal.hide();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $scope.refresh();

  };

  $scope.deleteGallery = function(gallery) {

      GalleryService.deleteGallery(gallery)
      .then(function(deletedGallery) {
        //console.log("deletedGallery: " + JSON.stringify(deletedGallery));
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $scope.refresh();
  };

  $scope.addGallery = function() {
      console.log("newGallery: " + $scope.newGallery);
      GalleryService.addGallery(PATIENT.PATIENTID, $scope.newGallery)
      .then(function(addedGallery) {
        //console.log("addedGallery: " + JSON.stringify(addedGallery));
        $scope.addModal.hide();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $scope.refresh();
  };

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadGallery();
      });
  } else {
        // load a refresh from the cloud
        $scope.loadGallery();
  }

});
