angular.module('doularoom.controllers.chart', [])

.controller('ChartCtrl', function ($rootScope, $scope, InitBluemix, ChartService, PATIENT, $stateParams) {

    $scope.loadCharts = function () {

        ChartService.getLatestBloodPressureData($stateParams.patientID)
        .then(function (latestBloodPressureData) {
            $scope.latestBloodPressureData = latestBloodPressureData;
            //console.log("latestBloodPressureData: " + JSON.stringify(latestBloodPressureData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekBloodPressureData($stateParams.patientID)
        .then(function (currentWeekBloodPressureData) {
            $scope.currentWeekBloodPressureData = currentWeekBloodPressureData;
            //console.log("currentWeekBloodPressureData: " + JSON.stringify(currentWeekBloodPressureData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekWaterIntakeData($stateParams.patientID)
        .then(function (currentWeekWaterIntakeData) {
            $scope.currentWeekWaterIntakeData = currentWeekWaterIntakeData;
            //console.log("currentWeekWaterIntakeData: " + JSON.stringify(currentWeekWaterIntakeData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekHeartRateData($stateParams.patientID)
        .then(function (currentWeekHeartRateData) {
            $scope.currentWeekHeartRateData = currentWeekHeartRateData;
            //console.log("currentWeekHeartRateData: " + JSON.stringify(currentWeekHeartRateData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekWeightData($stateParams.patientID)
        .then(function (currentWeekWeightData) {
            $scope.currentWeekWeightData = currentWeekWeightData;
            //console.log("currentWeekWeightData: " + JSON.stringify(currentWeekWeightData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekStepsData($stateParams.patientID)
        .then(function (currentWeekStepsData) {
            $scope.currentWeekStepsData = currentWeekStepsData;
            //console.log("currentWeekStepsData: " + JSON.stringify(currentWeekStepsData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekActiveMinutesData($stateParams.patientID)
        .then(function (currentWeekActiveMinutesData) {
            $scope.currentWeekActiveMinutesData = currentWeekActiveMinutesData;
            //console.log("currentWeekActiveMinutesData: " + JSON.stringify(currentWeekActiveMinutesData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

        ChartService.getCurrentWeekCaloriesData($stateParams.patientID)
        .then(function (currentWeekCaloriesData) {
            $scope.currentWeekCaloriesData = currentWeekCaloriesData;
            //console.log("currentWeekCaloriesData: " + JSON.stringify(currentWeekCaloriesData));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

    };

    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function () {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadCharts();
        });
    } else {
        // load a refresh from the cloud
        $scope.loadCharts();
    }

    $scope.refresh = function () {
        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

});
