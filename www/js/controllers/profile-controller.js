angular.module('doularoom.controllers.profile', [])

.controller('ProfileCtrl', function($rootScope, $scope, $location, InitBluemix, PATIENT, ProfileService) {

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.loadProfile = function() {

      ProfileService.getProfileData(PATIENT.PATIENTID)
      .then(function(profileData) {
        $scope.profileData = profileData;
        //console.log("profileData: " + JSON.stringify(profileData));
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });
  };

  $scope.updateProfile = function() {

      ProfileService.updateProfileData($scope.profileData)
      .then(function(savedData) {
        console.log("savedData: " + JSON.stringify(savedData));
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $location.path('/app/profile');
  };

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadProfile();
      });
  } else {
      // load a refresh from the cloud
      $scope.loadProfile();
  }

});
