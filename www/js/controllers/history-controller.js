angular.module('doularoom.controllers.history', [])

.controller('HistoryCtrl', function($rootScope, $scope, $location, InitBluemix, PATIENT, HistoryService) {

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.loadHistory = function() {

      HistoryService.getHistoryData(PATIENT.PATIENTID)
      .then(function(historyData) {
        $scope.historyData = historyData;
        console.log("historyData: " + JSON.stringify(historyData));
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });
  };

  $scope.updateHistory = function() {

      HistoryService.updateHistoryData($scope.historyData)
      .then(function(savedData) {
        console.log("savedData: " + JSON.stringify(savedData));
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $location.path('/app/dashboard');
  };

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadHistory();
      });
  } else {
        // load a refresh from the cloud
        $scope.loadHistory();
  }

});
