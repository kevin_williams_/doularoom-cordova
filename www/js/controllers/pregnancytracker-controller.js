angular.module('doularoom.controllers.pregnancytracker', [])

.controller('PregnancyTrackerCtrl', function($rootScope, $scope, InitBluemix, PregnancyTrackerService, PATIENT, $filter) {

  $scope.openWindow = function (url) {
        window.open(url, '_blank', 'location=yes');
    };

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.loadPregnancyTracker = function() {

      $scope.today = $filter('date')(new Date(), 'dd MMM');

      PregnancyTrackerService.getPregnancyTrackerData(PATIENT.PATIENTID)
      .then(function(pregnancyTrackerData) {
        $scope.pregnancyTrackerData = pregnancyTrackerData;
        //console.log("pregnancyTrackerData: " + JSON.stringify(pregnancyTrackerData));
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

  }

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadPregnancyTracker();
      });
  } else {
        // load a refresh from the cloud
        $scope.loadPregnancyTracker();
  }

});
