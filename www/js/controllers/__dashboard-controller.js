angular.module('doularoom.controllers.dashboard', [])

.controller('DashboardCtrl', function($rootScope, $scope, $ionicModal, InitBluemix, DashboardService,PATIENT, CalculationsService) {
    $scope.refresh = function () {
        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.newWeight = {};
    $scope.bloodPressure = {};
    $scope.heartRate = {};
    $scope.showMoves = false;
    $scope.showKicks = false;
    $scope.checkWeeks = 0;

    $ionicModal.fromTemplateUrl('templates/modals/add-water-intake.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.waterIntakeModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/modals/add-weight.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.weightModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/modals/add-blood-pressure.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.bloodPressureModal = modal;
    });

    $scope.addBloodPressure = function() {
        DashboardService.addBloodPressure(PATIENT.PATIENTID, $scope.bloodPressure)
        .then(function(savedBloodPressure) {
          console.log("savedBloodPressure: " + JSON.stringify(savedBloodPressure));
        },function(err){
          IBMBluemix.getLogger().warn(err);
        }).then(function () {
            $scope.getBloodPressure();

        });

        DashboardService.addHeartRate(PATIENT.PATIENTID, $scope.heartRate)
        .then(function(savedHeartRate) {
          console.log("savedHeartRate: " + JSON.stringify(savedHeartRate));
        },function(err){
          IBMBluemix.getLogger().warn(err);
        });

        $scope.bloodPressureModal.hide();
        $scope.refresh();
    };

    $scope.addWeight = function() {
        DashboardService.addWeight(PATIENT.PATIENTID, $scope.weightData)
        .then(function (savedWeight) {
            console.log("savedWeight: " + JSON.stringify(savedWeight));
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        }).then(function () {
            $scope.getWeight();
        });
        
        $scope.weightModal.hide();
        $scope.refresh();
    };

    $scope.addWaterData = function () {
        DashboardService.addWaterData(PATIENT.PATIENTID, $scope.waterData).then(function (savedWater) {
             console.log("savedWater: " + JSON.stringify(savedWater));
             //$scope.weightModal.hide();
         }, function (err) {
             IBMBluemix.getLogger().warn(err);
         }).then(function () {
             $scope.getWaterData();
         });
        $scope.waterIntakeModal.hide();
        $scope.refresh();
    };


    // Call the Service to Get the Information and update the $scope with what is required
    $scope.loadDashboard = function() {

        $scope.weightData = null;
        $scope.patientData = null;
     
        $scope.getPatient();
        $scope.getWeight();
        $scope.getBloodPressure();
        $scope.getHeartRate();
        $scope.getWaterData();
        $scope.getBabyKicks();
        $scope.getSteps();
        $scope.getSymptoms();
        

    }

    $scope.getPatient = function () {
        DashboardService.getPatient(PATIENT.PHYSICIANID, PATIENT.PATIENTID)
        .then(function (patientData) {
            $scope.patientData = patientData;
            $scope.refresh();
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });
    };
    $scope.getWeight = function () {
        DashboardService.getWeight(PATIENT.PATIENTID)
       .then(function (weightData) {
           $scope.weightData = weightData;
           $scope.refresh();
       }, function (err) {
           IBMBluemix.getLogger().warn(err);
       });
    };


    $scope.getBloodPressure = function () {
        DashboardService.getBloodPressure(PATIENT.PATIENTID)
        .then(function (bloodPressureData) {
            $scope.bloodPressure = bloodPressureData;
            $scope.refresh();
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });
    };

    $scope.getHeartRate = function () {
        DashboardService.getHeartRate(PATIENT.PATIENTID)
       .then(function (heartRateData) {
           $scope.heartRate = heartRateData;
           $scope.refresh();
       }, function (err) {
           IBMBluemix.getLogger().warn(err);
       });
    };

    $scope.getWaterData = function () {
        DashboardService.getWaterData(PATIENT.PATIENTID)
        .then(function (waterData) {
            $scope.waterData = waterData;
            $scope.refresh();
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });
    };

    $scope.getBabyKicks = function () {
        DashboardService.getBabyKicks(PATIENT.PATIENTID)
        .then(function (kicksData) {
            $scope.kicksData = kicksData;
            $scope.refresh();
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });
    };

    $scope.getSteps = function () {
        DashboardService.getSteps(PATIENT.PATIENTID)
        .then(function (stepsData) {
            $scope.stepsData = stepsData;
            $scope.refresh();
        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });
    };


    $scope.getSymptoms = function () {

        DashboardService.getSymptoms()
            .then(function (symptomsData) {
                $scope.symptomsData = symptomsData;
                $scope.refresh();
            }, function (err) {
                IBMBluemix.getLogger().warn(err);
            });
    };

    // Init Mobile Cloud SDK and wait for it to configure itself
    // Once complete keep a reference to it so we can talk to it later
    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function() {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadDashboard();
        });
    } else {
          // load a refresh from the cloud
          $scope.loadDashboard();
    }

});