angular.module('doularoom.controllers.device', [])

.controller('DeviceCtrl', function($rootScope, $scope, $state, $ionicModal, InitBluemix, PATIENT, TestResultsService) {

  $scope.devices = null;

  $ionicModal.fromTemplateUrl('templates/modals/view-paired-devices.html', {
      scope: $scope
  }).then(function(modal) {
      $scope.pairedDevicesModal = modal;
  });

  $scope.scanDevices = function() {
    $rootScope.scanningInfo = [];
    function scanDevicesCallback(data) {
      switch (data.action) {
        case "scanning":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
          });
          break;
        case "device-found":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
          });
          break;
        case "device-paired":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
          });
          break;
        case "blood-pressure-data":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
            $rootScope.scanningInfo.push(data.deviceData);
          });
          TestResultsService.addBloodPressure(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedBloodPressure) {
            console.log("savedBloodPressure: " + JSON.stringify(savedBloodPressure));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          TestResultsService.addHeartRate(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedHeartRate) {
            console.log("savedHeartRate: " + JSON.stringify(savedHeartRate));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          break;
        case "pedometer-data":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
            $rootScope.scanningInfo.push(data.deviceData);
          });
          TestResultsService.addSteps(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedSteps) {
            console.log("savedSteps: " + JSON.stringify(savedSteps));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          TestResultsService.addCalories(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedCalories) {
            console.log("savedCalories: " + JSON.stringify(savedCalories));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          TestResultsService.addActiveMinutes(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedActiveMinutes) {
            console.log("savedActiveMinutes: " + JSON.stringify(savedActiveMinutes));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          break;
        case "weight-scale-data":
          $rootScope.$apply(function () {
            $rootScope.scanningInfo.push(data.status);
            $rootScope.scanningInfo.push(data.deviceData);
          });
          TestResultsService.addWeight(PATIENT.PATIENTID, data.deviceData)
          .then(function(savedWeight) {
            console.log("savedWeight: " + JSON.stringify(savedWeight));
          },function(err){
            IBMBluemix.getLogger().warn(err);
          });
          break;
      }
    }

    LSPlugin.startScan(scanDevicesCallback);
    $state.go('app.scan');
  };

  $scope.stopScan = function() {
    function stopScanCallback(data) {
      if (navigator.notification){
        navigator.notification.alert(data.status, function(){}, 'Operation', 'Done');
      }
    }

    LSPlugin.stopScan(stopScanCallback);
  };

  $scope.listPairedDevices = function() {
    function retrievePairedDevicesCallback(data) {
      $scope.devices = data.devices;
      $scope.pairedDevicesModal.show();
    }

    LSPlugin.retrievePairedDevices(retrievePairedDevicesCallback);
  };

  $scope.forgetPairedDevices = function() {
    function forgetPairedDevicesCallback(data) {
      if (navigator.notification){
        navigator.notification.alert(data.status, function(){}, 'Operation', 'Done');
      }
    }

    LSPlugin.forgetPairedDevices(forgetPairedDevicesCallback);
  };

  $scope.deviceType = function(deviceEnum) {

    switch (deviceEnum) {
      case 0:
        return "Unknown";
        break;
      case 1:
        return "Weight Scale";
        break;
      case 4:
        return "Pedometer";
        break;
      case 5:
        return "Blood Pressure Monitor";
        break;
    }

  };

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          //$scope.loadProfile();
      });
  } else {
      // load a refresh from the cloud
      //$scope.loadProfile();
  }

});
