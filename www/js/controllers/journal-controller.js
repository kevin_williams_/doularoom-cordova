angular.module('doularoom.controllers.journal', [])

.controller('JournalCtrl', function($rootScope, $scope, $window, InitBluemix, JournalService) {

    $scope.Journal = {};

  $scope.shareItem = function (a, b, c, d) {
      window.plugins.socialsharing.share(a, b, c, d);
  };

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.createJournal= function(Image) {
      //alert($scope.Journal.PHOTO);
      //alert("Verify");
      //alert("Image: " + Image);
      //alert("Image 2" + document.getElementById("journalImageName").value);
      //alert("Image Name: " + $scope.Journal.PHOTO);
      JournalService.createJournal(window.sessionStorage.profileId, $scope.Journal, document.getElementById("journalImageName").value)
      .then(function(savedJournal) {
      },function(err){
        IBMBluemix.getLogger().warn(err);
      }).then(function () {
          document.getElementById('smallImage').src = "";
          alert("The Journal Was Saved Successfully");
          document.getElementById('smallImage').hidden = true;
          $scope.journalData = ''; //not yet tested to clear field
            $scope.loadJournal();
      });
      
      //$window.location.reload();
      //$scope.refresh();
      //$window.location.href = '#/app/journals';
      //$scope.Journal = {};
     

  };
    
    $scope.journalCapturePhoto = function(){
        //alert("Test");
        navigator.camera.getPicture(JournalService.onPhotoDataSuccess, JournalService.onFail, { quality: 50, destinationType: Camera.DestinationType.FILE_URI });   
    };

  $scope.loadJournal= function() {

      JournalService.getJournalData(window.sessionStorage.profileId, 0, 10)
      .then(function(journalData) {
        $scope.journalData = journalData;
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

  };

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadJournal();
      });
  } else {
        // load a refresh from the cloud
        $scope.loadJournal();
  }

});

