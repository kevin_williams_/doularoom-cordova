
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
		app.receivedEvent('deviceready');

         bluemix = IBMBluemix.hybrid;
         ibmpush = IBMPush.hybrid;

        values = {
                version: "1.0.0",
                applicationId: "09f0cb84-cee1-4dfa-b8bf-40b279be18a4",
                applicationRoute: "MaternAppCDev.mybluemix.net",
                applicationSecret: "c9b99f08b1d0202b1ca8d7d090c0d7bb4d85c583"
         };

        var push;
        var tagToSubscribe;
        bluemix.initialize(values). then(function() {

        	app.updateText("initialize completed successfully");

        	return ibmpush.initializeService();
        }). then(function (pushObj) {

        	push = pushObj;
        	app.updateText("Push Service initialized");
        	debugger;
        	return push.registerDevice("CordovaDevice","User1", "app.alertNotification");
        }).then(function(response) {

        	app.updateText("Successfully registered. " + response.deviceId);

			return push.getTags();
        }).then (function(response) {

        	app.updateText("Got tags successfully "+ JSON.stringify(response));

		    var tagList = response.tags;

		    if(tagList != null) {
		    	console.log(tagList[0]);
		    	tagToSubscribe = tagList[0];
		    }

			return push.subscribeTag(tagToSubscribe);
        }).then(function(response) {

        	app.updateText("Subscribed successfully "+ JSON.stringify(response));

        	return push.getSubscriptions();

        }).then(function(response) {

        	app.updateText("Got subscriptions successfully "+ JSON.stringify(response));

        	return push.unsubscribeTag(tagToSubscribe);

        }). then(function (response){
        	app.updateText("Unsubscribed successfully "+ JSON.stringify(response));

        }, function(error) {

        	app.updateText("Operation failed. Error message : "+error);
        });

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

		parentElement.setAttribute('style', 'display:none;');

        console.log('Received Event: ' + id);
    },

      updateText : function(text) {
    	  var parentElement = document.getElementById('resultarea');
    	  var content = document.createTextNode(text);
    	  parentElement.appendChild(content);
    	  parentElement.appendChild(document.createElement("br"));
    },

    alertNotification : function(message) {

          var parentElement = document.getElementById('resultarea');
    	  var content = document.createTextNode("Received notification : " + message.alert);
    	  parentElement.appendChild(content);
    	  parentElement.appendChild(document.createElement("br"));

    	  alert(JSON.stringify(message));
     }
};
