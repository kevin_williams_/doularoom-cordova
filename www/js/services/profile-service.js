angular.module('doularoom.services.profile', [])

.factory('ProfileService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getProfileData: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find({ PATIENTID: patientID})
      .then (function (_result) {

        var returnedObj = {};
        returnedObj.patient = _result[0].attributes;
        //console.log("returnedObj: " + JSON.stringify(returnedObj));
        return returnedObj;

      }).then(function(_result){

        var returnedObj = _result;
        return ibmData.Query.ofType(CLASS_NAMES.MY_GOALS)
        .find({ PATIENTID: returnedObj.patient.PATIENTID })
        .then(function (_result) {

          _result.forEach(function(goal){
            returnedObj[goal.attributes.GOAL] = goal.attributes;
          });
          //console.log("returnedObj: " + JSON.stringify(returnedObj));
          return defer.resolve(returnedObj);

        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    updateProfileData: function(profileData){
      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var returnedObj = {};

      //console.log("profileData: " + JSON.stringify(profileData));

      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find({ PATIENTID: profileData.patient.PATIENTID})
      .then (function (patients) {

        var patient = patients[0];
        patient.set(profileData.patient);
        return patient.save().then(function(savedPatient){
          returnedObj.savedPatient = savedPatient.attributes;
          //console.log("returnedObj: " + JSON.stringify(returnedObj));
          return returnedObj;
        });

      }).then(function(_result){

        var returnedObj = _result;
        return ibmData.Query.ofType(CLASS_NAMES.MY_GOALS)
        .find({ PATIENTID: profileData.patient.PATIENTID, GOAL:profileData.WATERINTAKE.GOAL })
        .then(function (goals) {

          var goal = goals[0];
          goal.set(profileData.WATERINTAKE);
          return goal.save().then(function(savedGoal){
            returnedObj.WATERINTAKE = savedGoal.attributes;
            //console.log("returnedObj: " + JSON.stringify(returnedObj));
            return returnedObj;
          });

        });

      }).then(function(_result){

        var returnedObj = _result;
        return ibmData.Query.ofType(CLASS_NAMES.MY_GOALS)
        .find({ PATIENTID: profileData.patient.PATIENTID, GOAL:profileData.STEPS.GOAL })
        .then(function (goals) {

          var goal = goals[0];
          goal.set(profileData.STEPS);
          return goal.save().then(function(savedGoal){
            returnedObj.STEPS = savedGoal.attributes;
            //console.log("returnedObj: " + JSON.stringify(returnedObj));
            return returnedObj;
          });

          return defer.resolve(returnedObj);

        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });


      return defer.promise;
    }

  };

});
