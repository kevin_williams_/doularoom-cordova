angular.module('doularoom.services.pregnancytracker', [])

.factory('PregnancyTrackerService', function($rootScope, $q, CLASS_NAMES, CalculationsService) {

  return {

    getPregnancyTrackerData: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find({ PATIENTID: patientID})
      .then (function (_result) {

        var returnedObj = {};
        returnedObj.pregnancyInWeeks = CalculationsService.getWeeksElapsed(new Date(), _result[0].attributes.LAST_MEN_CYCLE); //
        //returnedObj.pregnancyInWeeks = CalculationsService.getWeeksElapsed(new Date(), '2014-05-10T05:00:00.000Z');
        console.log("last_menstrual", _result[0].attributes.LAST_MEN_CYCLE);
        return returnedObj;

      }).then(function(_result){

        var returnedObj = _result;
        return ibmData.Query.ofType(CLASS_NAMES.WEEKLY_ADVICE)
        .find({ WEEK: returnedObj.pregnancyInWeeks })
        .then(function (_result) {

          returnedObj.weeklyAdvices = [];

          if (_result.length !== 0) {
            _result.forEach(function(weeklyAdvice){
              returnedObj.weeklyAdvices.push(weeklyAdvice.attributes);
            });
          }

          return returnedObj;

        });

      }).then(function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.WEEKLY_ARTICLES)
        .find({ WEEK: returnedObj.pregnancyInWeeks })
        .then(function (_result) {

          returnedObj.weeklyArticles = [];

          if (_result.length !== 0) {
            _result.forEach(function(weeklyArticle){
              returnedObj.weeklyArticles.push(weeklyArticle.attributes);
            });
          }

          return defer.resolve(returnedObj);

        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    }

  };

});
