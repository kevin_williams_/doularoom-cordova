angular.module('doularoom.services.journal', [])

.factory('JournalService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getJournalData: function(patientID, startNum, endNum){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.PREGNANCY_JOURNAL)
      .find({ PATIENTID: patientID})
      .then (function (_result) {

        //console.debug(_result);
        var _data = [];
        _result.forEach(function(obj){ _data.push(obj.attributes); });
        _data = _.sortBy(_data, function(obj) { return obj.TIME_RECORDED; });
        _data = _data.reverse();
        _data = _data.slice(startNum, endNum);

        return defer.resolve(_data);

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },
      
    onPhotoDataSuccess: function (imageData) {                
        // Get image handle
        var smallImage = document.getElementById('smallImage');
        var journalImageName = document.getElementById('journalImageName');
        // Unhide image elements
        smallImage.style.display = 'block';
        smallImage.src = imageData;
        journalImageName.value = imageData;
        //alert("Pre-Watch");
        //alert(imageData);
        //alert("Scope Data:" + $scope.Journal.JSUMMARY + " Photo:" + $scope.Journal.PHOTO);
        //JournalService.movePhoto(imageData);
    },
      
    movePhoto: function (file){
        alert("movePhoto: " + file);
        window.resolveLocalFileSystemURI( file , JournalService.resolveOnSuccess, JournalService.resOnError);
    },
      
    resolveOnSuccess: function (entry){
        
        var myDate = new Date();
        var fileName = myDate.getTime + ".jpg";
        var myJournalFolder = "Journal";
        alert("Filename:" + fileName);
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {      
        //The folder is created if doesn't exist
        fileSys.root.getDirectory( myJournalFolder,
                        {create:true, exclusive: false},
                        function(directory) {
                            entry.moveTo(directory, fileName,  function(new_entry){
                            path = new_entry.fullPath;
                            url = new_entry.toURL();

                            console.log(path+"\n"+url);

                            alert( path+"\n"+url );

                            jQuery('body').append('<img src="'+path+'" />');

                        }, JournalService.resOnError);
                    },
                    JournalService.resOnError);
            },
            JournalService.resOnError);
    },
      
    successMove: function (entry) {
        alert(entry.fullPath);
    },

    resOnError: function (error) {
        alert(error.code);
    },

  //Callback function when the picture has not been successfully taken
  onFail: function (message) {
    alert('Failed to load picture because: ' + message);
  },
    
    

    createJournal: function(patientID, Journal, ImageName){
      var defer = $q.defer();
      var ibmData = IBMData.getService();
        
      ibmData.Query.ofType(CLASS_NAMES.PREGNANCY_JOURNAL)
      .find()
      .then(function (_result) {

        _result = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('PJID');
        });

        _result = _result.sort(function(a, b){
          return a.attributes.PJID - b.attributes.PJID;
        });

        var returnedObj = {};
        returnedObj.latestPJID = _result[_result.length-1].attributes.PJID;
        console.log("_result: " + JSON.stringify(_result[_result.length-1].attributes));

        return returnedObj;

      }).then(function(_result){
          
        //alert("Documents: " + ImageName);
        var newJournal = {
          "PATIENTID": patientID,
          "TIME_RECORDED": (new Date()).toISOString(),
          "PHOTO": ImageName
        };
          
          
        /*var array = require('array-extended');
        var qry = req.ibmdata;
        var today = new Date();
        var path = require('path'),
        fs = require('fs');
        var tempPath = req.files.photo.path,
            targetPath = path.resolve('./public/img/journal/'+ req.files.photo.name);
        if (path.extname(req.files.photo.name).toLowerCase() === '.jpg') {
            fs.rename(tempPath, targetPath, function(err) {
                if (err) throw err;
                console.log("Upload completed!");
            });
        } else {
            fs.unlink(tempPath, function () {
                if (err) throw err;
                console.error("Only .jpg files are allowed!");
            });
        }*/
          
        var returnedObj = _result;
        var newPJID = ++returnedObj.latestPJID;
        var journal = ibmData.Object.ofType(CLASS_NAMES.PREGNANCY_JOURNAL, newJournal);

        journal.set({"PJID": newPJID, "JSUMMARY": Journal.JSUMMARY, "PHOTO": ImageName});
        return journal.save().then(function(savedJournal){
          returnedObj.savedJournal = savedJournal.attributes;
          return defer.resolve(returnedObj);
        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    }

  };

});
