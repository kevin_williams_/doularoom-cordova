angular.module('doularoom.services.gallery', [])

.factory('GalleryService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getGalleries: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.GALLERY_CATEGORIES)
      .find({ PATIENTID: patientID})
      .then (function (_result) {

        var returnedObj = {};
        returnedObj.categories = [];
        _result.forEach(function(category){
          returnedObj.categories.push(category.attributes);
        });
        return returnedObj;

      }).then(function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.PREGNANCY_GALLERY)
        .find({ PATIENTID: patientID })
        .then(function (_result) {

          returnedObj.pictures = [];
          _result.forEach(function(picture){
            returnedObj.pictures.push(picture.attributes);
          });
          //console.log("returnedObj: " + JSON.stringify(returnedObj));
          return returnedObj;

        });

      }).then(function(_result){

        var returnedObj = [];
        _result.categories.forEach(function(category){
          var pictures = _.filter(_result.pictures, function(picture){
            return category.PGID === picture.PGID;
          });
          category.pictures = pictures;
          returnedObj.push(category);
        });
        console.log("returnedObj: " + JSON.stringify(returnedObj));
        return defer.resolve(returnedObj);

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    updateGallery: function(Gallery){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.GALLERY_CATEGORIES)
      .find({ PGID: Gallery.PGID})
      .then (function (_result) {

        var returnedObj = {};
        var category = _result[0];
        category.set(Gallery);
        return category.save().then(function(savedGallery){
          returnedObj.savedGallery = savedGallery.attributes;
          //console.log("savedGallery: %s", savedGallery.attributes);
          return defer.resolve(returnedObj);
        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    deleteGallery: function(Gallery){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.GALLERY_CATEGORIES)
      .find({ PGID: Gallery.PGID})
      .then (function (_result) {

        var returnedObj = {};
        var category = _result[0];
        return category.del().then(function(deletedGallery){
          returnedObj.deletedGallery = deletedGallery.attributes;
          //console.log("deletedGallery: %s", deletedGallery.attributes);
          return defer.resolve(returnedObj);
        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    addGallery: function(patientID, Gallery){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.GALLERY_CATEGORIES)
      .find()
      .then (function (_result) {

        var returnedObj = {};
        _result = _.sortBy(_result, function(obj){
          return obj.attributes.PGID;
        });
        returnedObj.lastPGID = _result[_result.length-1].attributes.PGID;

        return returnedObj;

      }).then(function(_result){

        var returnedObj = _result;
        var gallery = ibmData.Object.ofType(CLASS_NAMES.GALLERY_CATEGORIES, Gallery);
        var newPGID = ++returnedObj.lastPGID;
        gallery.set({"PGID": newPGID, "PATIENTID": patientID});
        return gallery.save().then(function(savedGallery){
          returnedObj.savedGallery = savedGallery.attributes;
          //console.log("savedGallery: %s", savedGallery.attributes);
          return defer.resolve(returnedObj);
        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    }

  };

});
