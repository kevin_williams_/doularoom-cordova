angular.module('doularoom.services.testresults', [])

.factory('TestResultsService', function($rootScope, $q, CLASS_NAMES) {

  return {

    addHeartRate: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var heartRateData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": "bpm",
        "TYPE_OF_TEST": "heart_rate",
        "RESULT": data.heartRate,
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, heartRateData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    },

    addBloodPressure: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var bloodPressureData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": "mmHg",
        "TYPE_OF_TEST": "blood_pressure",
        "RESULT": data.sistolic + "/" + data.diastolic,
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, bloodPressureData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    },

    addWeight: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var weightData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": "lbs",
        "TYPE_OF_TEST": "blood_pressure",
        "RESULT": data.weight,
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, weightData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    },

    addSteps: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var stepsData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": "steps",
        "TYPE_OF_TEST": "steps",
        "RESULT": (data.walked + data.runned),
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, stepsData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    },

    addCalories: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var caloriesData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": data.caloriesUnit,
        "TYPE_OF_TEST": "calories",
        "RESULT": data.calories,
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, caloriesData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    },

    addActiveMinutes: function(patientID, data){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      var activeMinutesData = {
        "PATIENTID": patientID,
        "UNIT_OF_MEASURE": data.exerciseTimeUnit,
        "TYPE_OF_TEST": "active_minutes",
        "RESULT": data.exerciseTime,
        "TIME_RECORDED": moment(data.date).format("YYYY-MM-DDTHH:mm:ss.sss")
      };

      var testResult = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, activeMinutesData);

      testResult.save().then(function(savedTestResult){

        defer.resolve(savedTestResult.attributes);

      },function(err){

        console.log(err);
        defer.reject(err);

      });

      return defer.promise;
    }

  };

});
