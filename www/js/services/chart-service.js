angular.module('doularoom.services.chart', [])

.factory('ChartService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getLatestBloodPressureData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var d1 = [[40,90],[60,90]],
          d2 = [[40,120],[80,120]],
          d3 = [[40,140],[90,140]],
          d4 = [[40,190],[100,190]];
      var options = {
          axisLabels: {
              show: true
          },
          xaxes: [{
              axisLabel: 'Diastolic',
              axisLabelPadding: 25,
              axisLabelUseCanvas: true,
              axisLabelFontSizePixels: 22,
              axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
              max: 250,
              axisLabel: 'Systolic',
              axisLabelPadding: 25,
              axisLabelUseCanvas: true,
              axisLabelFontSizePixels: 22,
              axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          grid: {
              hoverable: true
          },
          tooltip: true,
          tooltipOpts: {
              content: "Your Blood Pressure: %y / %x"
          },
          legend: {
            margin: 8,
            noColumns: 5
        }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "blood_pressure" })
      .then( function (_result) {

        var returnedObj = {};
        var filteredData = _.filter(_result, function(obj){ return obj.attributes.hasOwnProperty('TIME_RECORDED'); });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var tmpBloodPressureReading = sortedData[sortedData.length - 1].attributes.RESULT.split("/");
        var d5 = [[Number(tmpBloodPressureReading[1]), Number(tmpBloodPressureReading[0])]];
        var data = [
          {label:"High", data:d4, color:"#e74c3c", lines: {show: true,fill:true }},
          {label:"Above Normal", data:d3, color:"#f1c40f", lines: {show: true,fill:true }},
          {label:"Normal", data:d2, color:"#2ecc71", lines: {show: true,fill:true }},
          {label:"Low", data:d1, color:"#3498db", lines: {show: true,fill:true}},
          {label:"You", data:d5, color:"#000", points: {show: true, lineWidth: 6, radius: 6, symbol: "circle" }}
        ];

        returnedObj.data = data;
        returnedObj.options = options;
        console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekBloodPressureData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          max: 200,
          axisLabel: '',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "blood_pressure" })
      .then( function (_result) {

        var systolicTimeReadings = [],
          diastolicTimeReadings = [];

        var filteredData = _.filter(_result, function(obj){ return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().valueOf(); });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED).format('YYYY-MM-DD'); });
        var bloodPressureTimeReadings = [];

        _.forEach(groupedData, function(v, k, c){
          var day = moment(k, 'YYYY-MM-DD').valueOf();
          var sumSystolic = _.reduce(v, function(result, num, key) {
            result += Number(num.attributes.RESULT.split("/")[0]);
            return result;
          }, 0);
          var sumDiastolic = _.reduce(v, function(result, num, key) {
            result += Number(num.attributes.RESULT.split("/")[1]);
            return result;
          }, 0);
          systolicTimeReadings.push([day, (sumSystolic/v.length)]);
          diastolicTimeReadings.push([day, (sumDiastolic/v.length)]);
        });

        //console.log("systolicTimeReadings: " + JSON.stringify(systolicTimeReadings));
        //console.log("diastolicTimeReadings: " + JSON.stringify(diastolicTimeReadings));

        var data = [
            {
                label:"Diastolic",
                data:diastolicTimeReadings,
                color:"#2ecc71",
                points: { show: true },
                lines:{show:true, fill:false}
            },
            {
                label:"Systolic",
                data:systolicTimeReadings,
                color:"#3498db",
                points: { show: true },
                lines:{show:true, fill:false}
            }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekHeartRateData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          max: 200,
          axisLabel: 'Heart Rate (bpm)',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
          tooltip: true,
          tooltipOpts: {
              content: "%s | X: %x | Y: %y"
          }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "heart_rate" })
      .then( function (_result) {

        var returnedObj = {},
          heartRateTimeReadings = [];

        var filteredData = _.filter(_result, function(obj){ return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf(); });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED).format('YYYY-MM-DD'); });

        _.forEach(groupedData, function(v, k, c){
          var day = moment(k, 'YYYY-MM-DD').valueOf();
          var sum = _.reduce(v, function(result, num, key) {
            result += Number(num.attributes.RESULT);
            return result;
          }, 0);
          heartRateTimeReadings.push([day, (sum/v.length)]);
        });
        //console.log("_result: " + JSON.stringify(heartRateTimeReadings));
        var data = [
          {
            label:"Heart Rate",
            data:heartRateTimeReadings,
            color:"#f1c40f",
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekWaterIntakeData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          //max: 10,
          axisLabel: 'Water Intake (ml)',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "water_intake" })
      .then( function (_result) {

        var waterIntakeTimeReadings = [];
        var filteredData = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf();
        });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED).format('YYYY-MM-DD'); });

        _.forEach(groupedData, function(v, k, c){
          var day = moment(k, 'YYYY-MM-DD').valueOf();
          var sum = _.reduce(v, function(result, num, key) {
            result += Number(num.attributes.RESULT);
            return result;
          }, 0);
          waterIntakeTimeReadings.push([day, sum]);
        });

        var data = [
          {
            label: "Water Intake",
            data: waterIntakeTimeReadings,
            shadowSize:0,
            color: '#2c3e50',
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekWeightData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          //max: 10,
          axisLabel: 'Weight (lbs)',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "weight" })
      .then( function (_result) {

        var weightTimeReadings = [];
        var filteredData = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf();
        });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED).format('YYYY-MM-DD'); });

        _.forEach(groupedData, function(v, k, c){
          var day = moment(k, 'YYYY-MM-DD').valueOf();
          var sum = _.reduce(v, function(result, num, key) {
            result += Number(num.attributes.RESULT);
            return result;
          }, 0);
          weightTimeReadings.push([day, (sum/v.length)]);
        });

        var data = [
          {
            label: "Weight",
            data: weightTimeReadings,
            shadowSize:0,
            color: '#f35958',
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekStepsData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          //max: 10,
          axisLabel: 'Steps',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "steps" })
      .then( function (_result) {

        var stepsTimeReadings = [];
        var filteredData = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf();
        });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED, 'YYYY-MM-DD').valueOf(); });

        _.forEach(groupedData, function(v, k, c){
          var day = Number(k);
          var sortedData = _.sortBy(v, function(obj) { return moment(obj.attributes.TIME_RECORDED).valueOf(); });
          var last = sortedData[sortedData.length-1].attributes.RESULT;
          stepsTimeReadings.push([day, last]);
        });

        var data = [
          {
            label: "Steps",
            data: stepsTimeReadings,
            shadowSize:0,
            color: '#2ecc71',
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekActiveMinutesData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          //max: 10,
          axisLabel: 'Active Minutes (min)',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "active_minutes" })
      .then( function (_result) {

        var activeMinutesTimeReadings = [];
        var filteredData = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf();
        });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED, 'YYYY-MM-DD').valueOf(); });

        _.forEach(groupedData, function(v, k, c){
          var day = Number(k);
          var sortedData = _.sortBy(v, function(obj) { return moment(obj.attributes.TIME_RECORDED).valueOf(); });
          var last = sortedData[sortedData.length-1].attributes.RESULT;
          activeMinutesTimeReadings.push([day, last]);
        });

        var data = [
          {
            label: "Active Minutes (min)",
            data: activeMinutesTimeReadings,
            shadowSize:0,
            color: '#3498db',
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    },

    getCurrentWeekCaloriesData: function(patientID) {

      var defer = $q.defer();
      var ibmData = IBMData.getService();
      var options = {
        axisLabels: {
            show: true
        },
          xaxes: [{
          axisLabel: 'Day of the Week',
          mode: "time",
          minTickSize: [1, "day"],
          min: moment().startOf('week').valueOf(),
          max: moment().endOf('week').valueOf(),
          timeformat: "%a",
          timezone: "browser",
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
          yaxes: [{
          //max: 10,
          axisLabel: 'Calories (kcal)',
          axisLabelPadding: 25,
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 22,
          axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif'
          }],
      grid: {
      hoverable: true,
      clickable: false
      },
      legend: {
      show: true,
      margin: 20
      },
            tooltip: true,
            tooltipOpts: {
                content: "%s | X: %x | Y: %y"
            }
      };

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ PATIENTID: patientID, TYPE_OF_TEST: "calories" })
      .then( function (_result) {

        var caloriesTimeReadings = [];
        var filteredData = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('TIME_RECORDED') && moment(obj.attributes.TIME_RECORDED).valueOf() > moment().startOf('week').valueOf() && moment(obj.attributes.TIME_RECORDED).valueOf() < moment().endOf('week').valueOf();
        });
        var sortedData = _.sortBy(filteredData, function(obj){ return obj.attributes.TIME_RECORDED; });
        var groupedData = _.groupBy(sortedData, function(obj){ return moment(obj.attributes.TIME_RECORDED, 'YYYY-MM-DD').valueOf(); });

        _.forEach(groupedData, function(v, k, c){
          var day = Number(k);
          var sortedData = _.sortBy(v, function(obj) { return moment(obj.attributes.TIME_RECORDED).valueOf(); });
          var last = sortedData[sortedData.length-1].attributes.RESULT;
          caloriesTimeReadings.push([day, last]);
        });

        var data = [
          {
            label: "Calories (kcal)",
            data: caloriesTimeReadings,
            shadowSize:0,
            color: '#8e44ad',
            bars: { show: true, align: "left", barWidth: 60*60*1000*24 }
          }
        ];

        var returnedObj = {};
        returnedObj.data = data;
        returnedObj.options = options;
        //console.log("_result: " + JSON.stringify(returnedObj));
        defer.resolve(returnedObj);

      }, function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    }


  };

});
