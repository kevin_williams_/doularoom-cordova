angular.module('doularoom.services.history', ['mgo-angular-wizard'])

.factory('HistoryService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getHistoryData: function(patientID){

      var emptyPatientObj = {
        FIRST_NAME: "",
        LAST_NAME: "",
        DOB: "",
        EMAIL: "",
        ADDRESS: "",
        CITY: "",
        STATE: "",
        COUNTRY: "",
        POSTAL_CODE: "",
        HOME_PHONE: "",
        NUM_OF_PREG: "",
        LAST_MEN_CYCLE: "",
        PATIENTID: "",
        RF_HYPER: "",
        RF_DIABETES: "",
        RF_PREV_UTER_SURG: "",
        RF_PFMG: "",
        RF_DRUG_USE: "",
        RF_PFD: "",
        RF_STD_STI: "",
        RF_HIV: "",
        RF_OVR_35_YOA: "",
        RF_SICKLE_CELL: "",
        DELIVERY_TYPE: "",
        BLOOD_TYPE: "",
        OCCUPATION: ""
      },
      emptyHospitalObj = {
        HNAME: "",
        ADDRESS: "",
        CITY: "",
        STATE: "",
        COUNTRY: "",
        POSTAL_CODE: "",
        PHONE: ""
      },
      defer = $q.defer(),
      ibmData = IBMData.getService();


      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find()
      .then (function (_result) {

        var returnedObj = {};
        _result = _.sortBy(_result, function(obj){
          return obj.attributes.PATIENTID;
        });
        returnedObj.lastPatientID = (_result.length !== 0)? _result[_result.length-1].attributes.PATIENTID : null;
        return returnedObj;

      }).then(function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.HOSPITALS)
        .find()
        .then (function (_result) {

          _result = _.sortBy(_result, function(obj){
            return obj.attributes.HOSID;
          });
          returnedObj.lastHospitalID = (_result.length !== 0)? _result[_result.length-1].attributes.HOSID : null;

          return returnedObj;

        });

      }).then (function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.HOSPITAL_PATIENTS)
        .find({ PATIENTID: patientID, PREFERRED: true })
        .then (function (_result) {

          returnedObj.hospitalID = (_result.length !== 0)? _result[0].attributes.HOSID : null;
          return returnedObj;

        });

      }).then (function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.HOSPITALS)
        .find({ HOSID: returnedObj.hospitalID })
        .then( function (_result) {

          returnedObj.hospital = (_result.length !== 0)? _result[0].attributes : emptyHospitalObj;
          return returnedObj;

        });

      }).then(function(_result){

        var returnedObj = _result;

        return ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
        .find({ PATIENTID: patientID })
        .then( function (_result) {

          returnedObj.patient = (_result.length !== 0)? _result[0].attributes : emptyPatientObj;
          return defer.resolve(returnedObj);

        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    updateHistoryData: function(historyData) {

      var defer = $q.defer(),
        ibmData = IBMData.getService();
      var num = Number(historyData.lastPatientID.match(/PAT(\d{3,})/)[1]);
      var newPatientID = "PAT" + (++num);
      var num = Number(historyData.lastHospitalID.match(/HOS(\d{3,})/)[1]);
      var newHospitalID = "HOS" + (++num);;

      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find({"PATIENTID": historyData.patient.PATIENTID})
      .then(function(patients){

        var returnedObj = {};
        returnedObj.currentPatient = null;

        if (patients.length !== 0) {
          var patient = patients[0];
          patient.set(historyData.patient);
          return patient.save().then(function(savedPatient){
            returnedObj.currentPatient = savedPatient.attributes;
            //console.log("PATIENT: %s", savedPatient.attributes);
            return returnedObj;
          });
        } else {
          var patient = ibmData.Object.ofType(CLASS_NAMES.PATIENTS, historyData.patient);

          patient.set({"PATIENTID": newPatientID});
          return patient.save().then(function(savedPatient){
            returnedObj.currentPatient = savedPatient.attributes;
            //console.log("PATIENT: %s", savedPatient.attributes);
            return returnedObj;
          });
        }

      }).then(function(result){

        var returnedObj = result;

        return ibmData.Query.ofType(CLASS_NAMES.HOSPITALS)
        .find({ HOSID: historyData.hospital.HOSID })
        .then(function(hospitals){

          if (hospitals.length !== 0) {
            var hospital = hospitals[0];
            hospital.set(historyData.hospital);
            return hospital.save().then(function(savedHospital){
              returnedObj.currentHospital = savedHospital.attributes;
              //console.log("Hospital: %s", savedHospital.attributes);
              return returnedObj;
            });
          } else {
            var hospital = ibmData.Object.ofType(CLASS_NAMES.HOSPITALS, historyData.hospital);

            hospital.set({"HOSID": newHospitalID});
            return hospital.save().then(function(savedHospital){
              returnedObj.currentHospital = savedHospital.attributes;
              //console.log("Hospital: %s", savedHospital.attributes);
              return returnedObj;
            });
          }

        });

      }).then(function(result){

        var returnedObj = result;

        return ibmData.Query.ofType(CLASS_NAMES.HOSPITAL_PATIENTS)
        .find({ "HOSID": returnedObj.currentHospital.HOSID, "PATIENTID": returnedObj.currentPatient.PATIENTID })
        .then(function(hospitalPatients){

          if (hospitalPatients.length !== 0) {
            var hospitalPatient = hospitalPatients[0];
            hospitalPatient.set({"PREFERRED": true});
            return hospitalPatient.save().then(function(savedHospitalPatient){
              //console.log("savedHospitalPatient: %s", JSON.stringify(savedHospitalPatient));
              returnedObj.hospitalPatient = savedHospitalPatient.attributes;
              return returnedObj;
            });
          } else {
            var hospitalPatient = ibmData.Object.ofType(CLASS_NAMES.HOSPITAL_PATIENTS);

            hospitalPatient.set({
              "HOSID": returnedObj.currentHospital.HOSID,
              "PATIENTID": returnedObj.currentPatient.PATIENTID,
              "PREFERRED": true
            });
            return hospitalPatient.save().then(function(savedHospitalPatient){
              //console.log("savedHospitalPatient: %s", JSON.stringify(savedHospitalPatient));
              returnedObj.hospitalPatient = savedHospitalPatient.attributes;
              return defer.resolve(returnedObj);
            });
          }

        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;

    }

  };

});
