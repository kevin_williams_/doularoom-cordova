var ds = angular.module('doularoom.services.dashboard', []);

/**
 * A Service that intialises MBaaS
 */
ds.factory('DashboardService', function ($rootScope, $q, CLASS_NAMES, $cacheFactory) {

  return {


    getPatient: function (physicianID, patientID) {
      //var cache = $cacheFactory('_patient_info');
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.PATIENTS)
      .find({ "PHYSICIANID": physicianID, "PATIENTID": patientID })
      .then(function(_result){
        defer.resolve(_result[0].attributes);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    getWeight: function (patientID) {
      //var cache = $cacheFactory('_weight');
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, TYPE_OF_TEST: "weight" })
      .then(function (_weights) {
          _weights = _.filter(_weights, function (obj) { return obj.attributes.hasOwnProperty('TIME_RECORDED'); });
         var _result = _.sortBy(_weights, function (obj) { return obj.attributes.TIME_RECORDED; });
         var object = {};
          //TODO change back values to 2 & 1 respectively for previous and present values
        object.yesterday = _result[_result.length - 2].attributes;
        object.latest = _result[_result.length - 1].attributes;
        console.log("weight_info", object);
        //cache.put(object);
        defer.resolve(object);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    addWeight: function(patientID, Weight){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

        var newWeight = {
          "PATIENTID": patientID,
          "UNIT_OF_MEASURE": "lbs",
          "RESULT": Weight.latest.RESULT,
          "TYPE_OF_TEST": "weight",
          "TIME_RECORDED": new Date()
        };

        var weight = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, newWeight);
        var returnedObj = {};
        var newTSTNUM = ++returnedObj.latestTSTNUM;

        //weight.set({"RESULT": Weight.RESULT, "TSTNUM": newTSTNUM});
        return weight.save().then(function(savedWeight){
            returnedObj.savedWeight = savedWeight.attributes;
            console.log("savedWeight", savedWeight.attributes);
          return defer.resolve(returnedObj);
        }, function(err){
            console.log(err);
            defer.reject(err);
        });

      

      return defer.promise;
    },

    getBloodPressure: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, TYPE_OF_TEST: "blood_pressure" })
      .then(function (_result) {
          _result = _.filter(_result, function (obj) { return obj.attributes.hasOwnProperty('TIME_RECORDED'); });
          _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
          console.log("blood_pressure", _result[_result.length - 1].attributes);
        defer.resolve(_result[_result.length-1].attributes);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    addBloodPressure: function(patientID, BP){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

        var returnedObj = {};
        //returnedObj.latestTSTNUM = _result[_result.length-1].attributes.TSTNUM;
        //console.log("_result: " + JSON.stringify(_result[_result.length-1].attributes));

        //return returnedObj;

        var newBloodPressure = {
          "PATIENTID": patientID,
          "UNIT_OF_MEASURE": "mmHg",
          "RESULT": BP.sistolic + "/" + BP.diastolic,
          "TYPE_OF_TEST": "blood_pressure",
          "TIME_RECORDED": new Date()
        };

        var bloodPressure = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, newBloodPressure);
        //var returnedObj = {};
        //var newTSTNUM = ++returnedObj.latestTSTNUM;
        //var result = BloodPressure.sistolic + "/" + BloodPressure.diastolic;

        //bloodPressure.set({"RESULT": result, "TSTNUM": newTSTNUM});
        return bloodPressure.save().then(function(savedBloodPressure){
            returnedObj.savedBloodPressure = savedBloodPressure.attributes;
            console.log("savedBloodPressure", savedBloodPressure.attributes);
          return defer.resolve(returnedObj);
        },function(err){
            console.log(err);
            defer.reject(err);
        });

      

      return defer.promise;
    },

    getHeartRate: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, TYPE_OF_TEST: "heart_rate" })
      .then(function (_result) {
        _result = _.sortBy(_result, function(obj){ return obj.attributes.TIME_RECORDED; });
        defer.resolve(_result[_result.length-1].attributes);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    addHeartRate: function(patientID, HR){
      var defer = $q.defer();
      var ibmData = IBMData.getService();


        var newHeartRate = {
          "PATIENTID": patientID,
          "UNIT_OF_MEASURE": "bpm",
          "TYPE_OF_TEST": "heart_rate",
          "RESULT": HR.RESULT,
          "TIME_RECORDED": new Date()
        };

        var heartRate = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, newHeartRate);
        var returnedObj = {};
        //var newTSTNUM = ++returnedObj.latestTSTNUM;

       // heartRate.set({"RESULT": HeartRate.RESULT, "TSTNUM": newTSTNUM});
        return heartRate.save().then(function(savedHeartRate){
          returnedObj.savedHeartRate = savedHeartRate.attributes;
          return defer.resolve(returnedObj);
        }, function(err){
            console.log(err);
            defer.reject(err);
        });

        return defer.promise;
    },

    getWaterData: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, "TYPE_OF_TEST": "water_intake" })
      .then(function (_result) {
          var _object = {};
          console.log("water_intake", _result);
        _result = _.sortBy(_result, function(obj){ return obj.attributes.TIME_RECORDED; });
        _object = _result[_result.length - 1].attributes;
        
        switch (_object.RESULT) {
            case 0:
                _object.intakeAmount = 0;
                _object.glasses = 8;
                break;
            case '0.25':
                _object.intakeAmount = 12.5;
                _object.glasses = 7;
                break;
            case 0.5:
                _object.intakeAmount = 25;
                _object.glasses = 6;
                break;
            case 0.75:
                _object.intakeAmount = 37.5;
                _object.glasses = 5;
                break;
            case 1:
                _object.intakeAmount = 50;
                _object.glasses = 4;
                break;
            case 1.25:
                _object.intakeAmount = 62.5;
                _object.glasses = 3;
                break;
            case 1.5:
                _object.intakeAmount = 75;
                _object.glasses = 2;
                break;
            case 1.75:
                _object.intakeAmount = 87.5;
                _object.glasses = 1;
                break;
            case 2:
                _object.intakeAmount = 100;
                _object.glasses = 0;
                break;
        }
        
        defer.resolve(_object);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    addWaterData: function (patientID, IntakeAmount) {
        var defer = $q.defer();
        var ibmData = IBMData.getService();
        var last_rec = null;
        var intake 
            intake = parseFloat(IntakeAmount.RESULT);
        console.log("AddWater", intake);

        ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, "TYPE_OF_TEST": "water_intake" })
      .then(function (_result) {
          console.log("water_intakeADD", _result);
          _result = _.sortBy(_result, function (obj) { return obj.attributes.TIME_RECORDED; });
           last_rec = _result[_result.length - 1].attributes.TIME_RECORDED;

           return last_rec;
      }).then(function (last_rec) {
          console.log("last_rec", last_rec);
          var inputDate = new Date(last_rec);
          var todaysDate = new Date();
          alert(inputDate.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0));
          //alert(inputDate);

          //call setHours to take the time out of the comparison
          if (inputDate.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0)) {

              if (intake < 2){
                  intake = intake + 0.25;
              } else {
                  intake = 2;
            }
          } else {
              intake = 0;
          }
          //Date equals today's date
          var waterIntakeData = {
              "PATIENTID": patientID,
              "UNIT_OF_MEASURE": "ml",
              "RESULT": intake,
              "TYPE_OF_TEST": "water_intake",
              "TIME_RECORDED": new Date()
          };

          var waterData = ibmData.Object.ofType(CLASS_NAMES.TEST_RESULTS, waterIntakeData);
          waterData.save().then(function (savedWaterIntake) {
              defer.resolve(savedWaterIntake.attributes);
          }, function (err) {

              console.log(err);
              defer.reject(err);

          });

          return defer.promise;
      });

        
    },
    getBabyKicks: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, TYPE_OF_TEST: "baby_kicks" })
      .then(function (_result) {
        _result = _.sortBy(_result, function(obj){ return obj.attributes.TIME_RECORDED; });
        defer.resolve(_result[_result.length-1].attributes);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    getSteps: function(patientID){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.TEST_RESULTS)
      .find({ "PATIENTID": patientID, TYPE_OF_TEST: "steps" })
      .then(function (_result) {
        _result = _.sortBy(_result, function(obj){ return obj.attributes.TIME_RECORDED; });
        defer.resolve(_result[_result.length-1].attributes);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    getSymptoms: function(){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.SYMPTOMS)
      .find()
      .then(function (_result) {
        var _symptomsData = [];
        _result.forEach(function(symptom){ _symptomsData.push(symptom.attributes); });
        defer.resolve(_symptomsData);
      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    }

  };

});
