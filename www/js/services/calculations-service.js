angular.module('doularoom.services.calculations', [])

.factory('CalculationsService', function() {

  return {
      estimatedConceptionDate: "",
      estimatedDueDate: "",
      estimatedFetalDate: "",

      isValidDate: function (dateStr) {
          //Checks for the following valid date formats:
          //MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY

          var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/; // requires 4 digit year

          var matchArray = dateStr.match(datePat);
          //Checking if the format of the date is valid?
          if (matchArray == null) {
              alert(" The date is not in a valid format.");
              return false;
          }

          var month = matchArray[1]; // parse date into variables
          var day = matchArray[3];
          var year = matchArray[4];
          if (month < 1 || month > 12) { // check month range
              alert("Month must be between 1 and 12.");
              return false;
          }

          if (day < 1 || day > 31) { // check day range
              alert("Day must be between 1 and 31.");
              return false;
          }

          if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) { // check for month that do not have 31 days
              alert("Month " + month + " doesn't have 31 days!")
              return false;
          }

          if (month == 2) { // check for february 29th (if current year is a leap year)
              var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
              if (day > 29 || (day == 29 && !isleap)) {
                  alert("February " + year + " doesn't have " + day + " days!");
                  return false;
              }
          }
          return true;
      },
      dispDate: function (dateObj) {
          var month = dateObj.getMonth() + 1;
          month = (month < 10) ? "0" + month : month;

          var day = dateObj.getDate();
          day = (day < 10) ? "0" + day : day;

          var year = dateObj.getYear();
          if (year < 2000) year += 1900;

          return (month + "/" + day + "/" + year);
      },
      calculateBMI_Non_Metric: function (weight, HeightFeetInt, HeightInchesInt) {
          var w = (weight * 1);
          var heightF = (HeightFeetInt * 1);
          var heightI = (HeightInchesInt * 1);
          var HeightFeetConvert = (heightF * 12);
          var h = (HeightFeetConvert + heightI);
          return Math.round((w * 703) / (h * h));
      },
      calculateBMI_Metric: function (weight, height) {
          return Math.floor(weight / (height * height));
      },
      validateCycle: function (avgMenstrualCycleLength) {
          var cycle = (avgMenstrualCycleLength == "" ? 28 : avgMenstrualCycleLength); // defaults to 28
          // validates cycle range, from 22 to 45
          if (avgMenstrualCycleLength != "" && (avgMenstrualCycleLength < 22 || avgMenstrualCycleLength > 45)) {
              alert("Your cycle length is either too short or too long for \n" + "calculations to be very accurate!  We will still try to \n" + "complete the calculation with the figure you entered. ");
          }

          return cycle;
      },
      validateLuteal: function (avgLutealPhaseLength) {
          var luteal = (avgLutealPhaseLength == "" ? 14 : avgLutealPhaseLength); // defaults to 14
          // validates luteal range, from 9 to 16
          if (avgLutealPhaseLength != "" && (avgLutealPhaseLength < 9 || avgLutealPhaseLength > 16)) {
              alert("Your luteal phase length is either too short or too long for \n" + "calculations to be very accurate!  We will still try to complete \n" + "the calculation with the figure you entered. ");
          }

          return luteal;
      },
      getDaysElapsed: function (greaterDate, lesserDate) {
          var greatDate = new Date(greaterDate);
          var lessDate = new Date(lesserDate);
          var diff = greatDate.getTime() - lessDate.getTime();
          var daysElapsed = (diff / (1000 * 60 * 60 * 24));
          return Math.floor(daysElapsed);

      },
      getWeeksElapsed: function (greaterDate, lesserDate) {
          var daysElapsed = this.getDaysElapsed(greaterDate, lesserDate);
          var weeks = (daysElapsed / 7);
          return Math.floor(weeks);

      },
      getTrimester: function (greaterDate, lesserDate) {
          var trimester = 0;
          var weeks = this.getWeeksElapsed(greaterDate, lesserDate);
          if ((weeks > 0) && (weeks <= 14)) {
              trimester = 1;
          } else if ((weeks > 14) && (weeks <= 28)) {
              trimester = 2;
          } else if ((weeks > 28)) {
              trimester = 3;
          }
          return Math.floor(trimester);
      },
      //NOTE: This function accepts only dates in the ISO date format. For example:  2014-05-10T05:00:00.000Z
      pregnancyCalc: function (lastMenstrualCycleDate, avgLengthOfCycles, avgLutealPhaseLength) {

		  lastMenstrualCycleDate = lastMenstrualCycleDate.split("T")[0];
		  var dateParts = lastMenstrualCycleDate.split("-");
		  //console.log("dateParts", dateParts)

		  var month_day_year = '' + dateParts[1] + '-' + dateParts[2] + '-' + dateParts[0] + '';
		  //console.log("month_day_year: " + month_day_year)

		  lastMenstrualCycleDate = month_day_year;

          var menstrual = new Date(); // creates new date objects
          var ovulation = new Date();
          var duedate = new Date();
          var today = new Date();
          var cycle = 0;
          var luteal = 0;

          if (this.isValidDate(lastMenstrualCycleDate)) { // Validates menstual date
              menstrualinput = new Date(lastMenstrualCycleDate);
              menstrual.setTime(menstrualinput.getTime());
          } else {
              return false; // otherwise exits
          }

          cycle = this.validateCycle(avgLengthOfCycles); //Validates the average length of mentrual cycle.

          luteal = this.validateLuteal(avgLutealPhaseLength); //Validates the average luteal-phase length.


          // sets ovulation date to menstrual date + cycle days - luteal days
          // the '*86400000' is necessary because date objects track time
          // in milliseconds;  86400000 milliseconds equals one day
          ovulation.setTime(menstrual.getTime() + (cycle * 86400000) - (luteal * 86400000));
          this.estimatedConceptionDate = this.dispDate(ovulation);

          // sets due date to ovulation date plus 266 days
          duedate.setTime(ovulation.getTime() + 266 * 86400000);
          this.estimatedDueDate = this.dispDate(duedate);

          // sets fetal age to 14 + 266 (pregnancy time) - time left
          var fetalage = 14 + 266 - ((duedate - today) / 86400000);
          weeks = parseInt(fetalage / 7); // sets weeks to whole number of weeks
          days = Math.floor(fetalage % 7); // sets days to the whole number remainder

          // fetal age message, automatically includes 's' on week and day if necessary
          fetalage = weeks + " week" + (weeks > 1 ? "s" : "") + ", " + days + " days";
          this.estimatedFetalDate = fetalage;

      }

  };

});
