// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic',
  'starter.controllers',
  'doularoom.controllers.dashboard',
  'doularoom.controllers.pregnancytracker',
  'doularoom.controllers.nearbyhospitals',
  'doularoom.controllers.journal',
  'doularoom.controllers.profile',
  'doularoom.controllers.gallery',
  'doularoom.controllers.history',
  'doularoom.controllers.device',
  'doularoom.controllers.chart',
  'doularoom.controllers.dashboardcharts',
  'starter.services',
  'doularoom.services.calculations',
  'doularoom.services.dashboard',
  'doularoom.services.pregnancytracker',
  'doularoom.services.journal',
  'doularoom.services.profile',
  'doularoom.services.gallery',
  'doularoom.services.history',
  'doularoom.services.testresults',
  'doularoom.services.chart',
  'ui.bootstrap',
  'doularoom.directives.chart'
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.run(function ($rootScope, $location, $state) {
        $rootScope.$on("$stateChangeStart", function (event, nextRoute, currentRoute) {

            if ((window.sessionStorage.loggedIn == "false" || window.sessionStorage.loggedIn == undefined)) {

                if ($state.current.name == "app.login") {
                    event.preventDefault();

                    //console.log($state.current.name);
                    $location.path("/app/login");
                }
            }
            else {
                //alert(currentRoute);
            }
        });
    })


.constant('PATIENT', {
    PHYSICIANID: "P100",
    PATIENTID: "PAT100"
})

.constant('CLASS_NAMES', {
    PATIENTS: 'PATIENTS',
    SYMPTOMS: 'SYMPTOMS',
    NUTRITION_INTAKE: 'NUTRITION_INTAKE',
    NUTRITION_INTAKE_DAILY: 'NUTRITION_INTAKE_DAILY',
    TEST_RESULTS: 'TEST_RESULTS',
    PAT_SYM: 'PAT_SYM',
    WEEKLY_ADVICE: 'WEEKLY_ADVICE',
    WEEKLY_ARTICLES: 'WEEKLY_ARTICLES',
    PREGNANCY_JOURNAL: 'PREGNANCY_JOURNAL',
    DEFAULT_PROFILE_PHOTOS: 'DEFAULT_PROFILE_PHOTOS',
    PROFILE_PHOTOS: 'PROFILE_PHOTOS',
    MY_GOALS: 'MY_GOALS',
    PREGNANCY_GALLERY: 'PREGNANCY_GALLERY',
    GALLERY_CATEGORIES: 'GALLERY_CATEGORIES',
    HOSPITALS: 'HOSPITAL',
    HOSPITAL_PATIENTS: 'HOS_PHY_PAT'
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent' :{
          templateUrl: "templates/profile.html",
          controller: 'ProfileCtrl',
          access: "true"
        }
      }
    })
	.state('app.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: "templates/about.html",
          controller: 'AboutCtrl',
          access: "true"
        }
      }
    })
    .state('app.scan', {
      url: "/devices/scan",
      views: {
        'menuContent' :{
          templateUrl: "templates/scan.html",
          controller: 'DeviceCtrl',
          access: "true"
        }
      }
    })
    .state('app.devices', {
      url: "/devices",
      views: {
        'menuContent' :{
          templateUrl: "templates/devices.html",
          controller: 'DeviceCtrl',
          access: "true"
        }
      }
    })
    .state('app.galleries', {
      url: "/galleries",
      views: {
        'menuContent' :{
          templateUrl: "templates/galleries.html",
          controller: 'GalleryCtrl',
          access: "true"
        }
      }
    })
	.state('app.calendar', {
      url: "/calendar",
      views: {
        'menuContent' :{
          templateUrl: "templates/calendar.html",
          controller: 'CalendarCtrl',
          access: "true"
        }
      }
    })
    .state('app.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: "templates/login.html",
            controller: 'LoginCtrl',
                access: { requiredLogin: false }
        }
      }
    })
    .state('app.galleries-manage', {
      url: "/galleries-manage",
      views: {
        'menuContent' :{
          templateUrl: "templates/galleries-manage.html",
          controller: 'GalleryCtrl',
          access: "true"
        }
      }
    })
    .state('app.charts', {
      url: "/charts/:patientID",
      views: {
        'menuContent' :{
            templateUrl: "templates/charts.html",
            controller: 'ChartCtrl',
            access: "true"
        }
      }
    })
    .state('app.history', {
      url: "/history",
      views: {
        'menuContent' :{
          templateUrl: "templates/history.html",
          controller: 'HistoryCtrl',
          access: "true"
        }
      }
    })
    .state('app.journals', {
      url: "/journals",
      views: {
        'menuContent' :{
          templateUrl: "templates/journals.html",
          controller: 'JournalCtrl',
          access: "true"
        }
      }
    })
    .state('app.helplinks', {
      url: "/helpful-links",
      views: {
        'menuContent' :{
            templateUrl: "templates/helpful-links.html",
            access: "true"
        }
      }
    })
    .state('app.complications', {
      url: "/complications",
      views: {
        'menuContent' :{
            templateUrl: "templates/complications.html",
            access: "true"
        }
      }
    })
    .state('app.otc-medication', {
      url: "/otc-medication",
      views: {
        'menuContent' :{
            templateUrl: "templates/otc-medication.html",
            access: "true"
        }
      }
    })
    .state('app.nearby-hospitals', {
      url: "/nearby-hospitals",
      views: {
        'menuContent' :{
          templateUrl: "templates/nearby-hospitals.html",
          controller: 'NearbyHospitalsCtrl',
          access: "true"
        }
      }
    })
    .state('app.tracker', {
      url: "/tracker",
      views: {
        'menuContent' :{
          templateUrl: "templates/tracker.html",
          controller: 'PregnancyTrackerCtrl',
          access: "true"
        }
      }
    })
    .state('app.dashboard', {
      url: "/dashboard",
      views: {
        'menuContent' :{
          templateUrl: "templates/dashboard.html",
          controller: 'DashboardCtrl',
          access: "true"
        }
      }
    })
    .state('app.weight', {
        url: "/weight-chart/:patientID", //updated with ":patientID" to pull individual records
        views: {
            'menuContent' :{
                templateUrl: "templates/weight-chart.html",
                controller: 'DashboardChartsCtrl',
                access: "true"
            }
        }
    })
    .state('app.heart-rate', {
        url: "/heart-rate/:patientID", //updated with ":patientID" to pull individual records
        views: {
            'menuContent' :{
                templateUrl: "templates/heart-rate-chart.html",
                controller: 'DashboardChartsCtrl',
                access: "true"
            }
        }
    })
    .state('app.blood-pressure', {
        url: "/blood-pressure-chart/:patientID", //updated with ":patientID" to pull individual records
        views: {
            'menuContent' :{
                templateUrl: "templates/blood-pressure-chart.html",
                controller: 'DashboardChartsCtrl',
                access: "true"
            }
        }
    })
    .state('app.water-intake', {
        url: "/water-intake-chart/:patientID", //updated with ":patientID" to pull individual records
        views: {
            'menuContent' :{
                templateUrl: "templates/water-intake-chart.html",
                controller: 'DashboardChartsCtrl',
                access: "true"
            }
        }
    })
    .state('app.fit', {
        url: "/fit-charts/:patientID", //updated with ":patientID" to pull individual records
        views: {
            'menuContent' :{
                templateUrl: "templates/fit-charts.html",
                controller: 'DashboardChartsCtrl',
                access: "true"
            }
        }
    })
    //Patient Roster
  .state('app.patient_roster', {
      url: "/patient_roster",
      views: {
          'menuContent' :{
              templateUrl: "templates/patient_roster.html",
              controller: 'patient_rosterCtrl',
              access: "true"
          }
      }
  })

   //Patient Vitamins
  .state('app.vitamins_intake', {
      url: "/vitamins_intake/:patientID",
      views: {
          'menuContent' :{
              templateUrl: "templates/vitamins_intake.html",
              controller: 'vitamins_intakeCtrl',
              access: "true"
          }
      }
  })

   //Patient weight
  .state('app.weight_info', {
      url: "/weight_info/:patientID", //updated with ":patientID" to pull individual records
      views: {
          'menuContent' :{
              templateUrl: "templates/weight_info.html",
              controller: 'weight_infoCtrl',
              access: "true"
          }
      }
  })
   //Page-Under-Construction
  .state('app.page_under_construction', {
      url: "/page_under_construction",
      views: {
          'menuContent': {
              templateUrl: "templates/page_under_construction.html",
              controller: 'page_under_constructionCtrl',
              access: "true"
          }
      }
  });

    // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/app/login');
});
