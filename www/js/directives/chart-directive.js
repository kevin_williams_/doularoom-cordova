angular.module('doularoom.directives.chart', [])

.directive('flotchart', function($timeout) {
  return {

    restrict: 'E',
    link: function(scope, elem, attrs){

      var chart = null;
      var chartParams = attrs.ngModel;

      scope.$watch(chartParams, function (value) {
        if (value) {
          if(!chart){
            chart = $.plot(elem, value.data , value.options);
          }else{
            chart.setData(v);
            chart.setupGrid();
            chart.draw();
          }
        }
      });

    }

  };

});
