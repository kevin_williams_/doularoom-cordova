angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    $scope.menuName = '';

    $scope.showMenu = function(menuName) {
        $scope.menuName = menuName;
    };

    angular.element(document.querySelectorAll('.sidebar-menu a.level-1-link, .sidebar-menu a.level-2-link'))
    .on('click', function(){
        $scope.menuName = '';
    });

    $scope.notifications = 0;

    


    $scope.subscribe = function(){

        // Vibrate the Phone
        navigator.notification.vibrate(2500);
    }
    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    },

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})


//Patient Roster Controller
.controller('patient_rosterCtrl', function ($rootScope, $scope, InitBluemix, patient_roster_Service, PATIENT) {

    $scope.refresh = function() {

        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }

    }

    // Call the Service to Get the Information and update the $scope with what is required
    $scope.loadPatientRoster = function() {

        // Access the Physician's Patients Data
        $scope.physiciansPatientData = null; // stores the data that is accessed by the html page.

        // Gets all patients
        //patient_roster_Service.getRoster(PATIENT.PHYSICIANID).then(function(physiciansPatientData) { //Original

        //patient_roster_Service.getRoster(PATIENT.PATIENTID).then(function(physiciansPatientData) { //For testing WORKING
        patient_roster_Service.getData2().then(function(physiciansPatientData) { //For testing
            $scope.physiciansPatientData = physiciansPatientData;
            $scope.refresh();

            //$route.reload(); //refreshes the page

        },function(err){
            IBMBluemix.getLogger().warn(err);
        });

    }

    // Init Mobile Cloud SDK and wait for it to configure itself
    // Once complete keep a reference to it so we can talk to it later
    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function() {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadPatientRoster();
        });
    } else {
        // load a refresh from the cloud
        $scope.loadPatientRoster();
    }

})

//End Patient Roster Controller

    /*.filter('date', function($filter)
    {
        return function(input)
        {
            if(input == null){ return ""; } 
 
            var _date = $filter('date')(new Date(input), 'MMM dd yyyy');
 
            return _date.toUpperCase();

        };
    })*/

//Vitamins Controller
.controller('vitamins_intakeCtrl', function($rootScope, $scope, InitBluemix, patient_vitamins_Service, PATIENT, $stateParams) {

    $scope.refresh = function() {

        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }

    }

    $scope.checkboxChecked="on";

    $scope.pid=null;
    $scope.setPatientID = function(pid) {

        $scope.pid=pid;

    }


    $scope.navigate_url = function (url) {
        window.open(url,"_self");
    };

    // Call the Service to Get the Information and update the $scope with what is required
    $scope.loadPatientVitamins = function() {

        // Access the Physician's Patients Data
        $scope.patientVitaminsData = null; // stores the data that is accessed by the html page.

        // Gets all patients
        //patient_roster_Service.getRoster(PATIENT.PHYSICIANID).then(function(patientVitaminsData) { //Original
          
        patient_vitamins_Service.getPatientVitamins($stateParams.patientID).then(function(patientVitaminsData) { //For testing
            console.log("State-Params", $stateParams);
            $scope.patientVitaminsData = patientVitaminsData;
            $scope.refresh();

            //$route.reload(); //refreshes the page
        },function(err){
            IBMBluemix.getLogger().warn(err);
        });

    }

    // Init Mobile Cloud SDK and wait for it to configure itself
    // Once complete keep a reference to it so we can talk to it later
    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function() {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadPatientVitamins();
        });
    } else {
        // load a refresh from the cloud
        $scope.loadPatientVitamins();
    }

})


    //Page-Under-Construction Controller
.controller('page_under_constructionCtrl', function ($rootScope, $scope, InitBluemix, page_under_construction_Service, PATIENT, $stateParams) {

   

  

  

})



.controller('LoginCtrl', function ($rootScope, $scope, $location, InitBluemix, LoginService) {

    $scope.refresh = function () {

        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }

    }

    $scope.processLogin = function () {
        // Access the Login Data
        $scope.loginData = null;
        //alert("Test");
        LoginService.getData($scope.username, $scope.password).then(function (loginData) {
            console.log("loginData", loginData[0].attributes.ROLE);
            //alert(JSON.stringify(loginData[0].attributes.ROLE));

            $scope.logIn($scope.username, $scope.password, loginData[0].attributes.ROLE);

        }, function (err) {
            IBMBluemix.getLogger().warn(err);
        });

    }

    $scope.logIn = function logIn(username, password, role) {
        if (username !== undefined && password !== undefined) {
            //alert("Username:" + username + ", Password:" + password + ", Role:" + role);
            switch (role) {
                case "Patient":
                    /*blockUI('.page-container');
                    window.setTimeout(function () {
                        unblockUI('.page-container');
                    }, 1000);*/
                    $location.path("/app/dashboard");
                    break;
                case "Physician":
                    /*blockUI('.page-container');
                    window.setTimeout(function () {
                        unblockUI('.page-container');
                    }, 1000);*/
                    $location.path("/app/patient_roster");

                    break;
            }

            window.sessionStorage.setItem("username", username);
            //window.sessionStorage.setItem("password", password);
            window.sessionStorage.setItem("role", role);
            window.sessionStorage.setItem("loggedIn", true);
        }
    }

    $scope.logOut = function logout() {
        window.sessionStorage.setItem("username", "");
        //window.sessionStorage.setItem("password", "");
        window.sessionStorage.setItem("role", "");
        window.sessionStorage.setItem("loggedIn", false);
        $location.path("/app/login");
    }
    // Call the Service to Get the Information and update the $scope with what is required
    $scope.loadLogin = function () {
        // Access the Login Data
        $scope.loginData = null;
        /*window.sessionStorage.setItem("username", "");
        //window.sessionStorage.setItem("password", "");
        window.sessionStorage.setItem("role", "");
        window.sessionStorage.setItem("loggedIn", false);*/

    }

    // Init Mobile Cloud SDK and wait for it to configure itself
    // Once complete keep a reference to it so we can talk to it later
    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function () {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadLogin();
        });
    } else {
        // load a refresh from the cloud
        $scope.loadLogin();
    }

})


//End vitamins Controller


//Weight Controller
.controller('weight_infoCtrl', function($rootScope, $scope, InitBluemix, patient_weight_Service, PATIENT) {

    $scope.refresh = function() {

        // Refresh
        if (!$scope.$$phase) {
            $scope.$apply();
        }

    }

    $scope.navigate_url = function (url) {
        window.open(url,"_self");
    };

    // Call the Service to Get the Information and update the $scope with what is required
    $scope.loadPatientWeight = function() {

        // Access the Physician's Patients Data
        $scope.patientWeightsData = null; // stores the data that is accessed by the html page.

        // Gets all patients
        //patient_roster_Service.getRoster(PATIENT.PHYSICIANID).then(function(patientWeightData) { //Original
          
        patient_weight_Service.getPatientWeight(PATIENT.PATIENTID).then(function(patientWeightsData) { //For testing
            $scope.patientWeightsData = patientWeightsData;          
            $scope.refresh();
            $route.reload(); //refreshes the page
        },function(err){
            IBMBluemix.getLogger().warn(err);
        });

    }

    // Init Mobile Cloud SDK and wait for it to configure itself
    // Once complete keep a reference to it so we can talk to it later
    if (!$rootScope.IBMBluemix) {
        InitBluemix.init().then(function() {
            $rootScope.IBMBluemix = IBMBluemix;
            $scope.loadPatientWeight();
        });
    } else {
        // load a refresh from the cloud
        $scope.loadPatientWeight();
    }

});

//End Weight Controller

